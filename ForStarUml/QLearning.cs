﻿// RL Project
// Ditolve Davide 806953
// Universita' degli studi Milano Bicocca
using RLDataLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using OfficeOpenXml;
using log4net.Repository.Hierarchy;

namespace Common
{
    public static class QLearning
    {
        private static int _states;// = (int)Math.Ceiling((double)((double)25000000 / (double)2500));//25000000;
        private static readonly int _actions = 9;
        private static double[,] _qTable;// = new double[States, Actions];

        private static double _lastDistanceFromGoal;
        private static double _distanceFromGoal;
        private static readonly ILog Log = CommonFeature.GetLogger();

        // Hyperparameters
        private static double _alpha = 0.3;

        private static double _gamma = 0.6;

        private static double _epsilon = 0.125;

        private static double _penalties = 0;
        private static double _reward = 0;
        private static int _actualState = 0;
        private static int _lastState = 0;
        private static int _lastAction = 0;
        private static readonly Random R = new Random();
        private static int _lastEpoch = 0;
        private static double _cumulativeReward = 0;
        private static readonly ExcelPackage Excel = new ExcelPackage(new FileInfo("C:\\Addon Projects\\TestFile\\qTable.xlsx"));

        private static double _oldValue;
        private static bool _checkpoint;
        private static int _goalEpisode;

        public static double ValueToSave;

        private static double minSpeed, maxSpeed;
        public static double MinSpeed; 
        public static double MaxSpeed; 

        public static int States;


        private static int _tilePerRow;
        private static bool _outOfPath;
        private static bool _goalReached;
        public static int Actions;

        public static double[,] QTable;
        public static double Epsilon;
        public static bool Checkpoint;
        public static double Gamma; 
        public static double Alpha; 
        public static bool obstacleHit;
        public static int TilePerRow; 
        public static bool OutOfPath; 
        public static bool GoalReached;
        public static int GoalEpisode;

        private static FixedSizedQueue<int> actionsBuffer = new FixedSizedQueue<int>(3);
        private static FixedSizedQueue<double> rewardsBuffer = new FixedSizedQueue<double>(3);
        public static void SetLastEpoch(int epoch)
        {}

        public static double GetEpsilon()
        {
            return Epsilon;
        }
        public static double GetCumulativeReward()
        {
            return _cumulativeReward;
        }
        public static double GetLastReward()
        {
            return _reward;
        }
        public static void resetReward()
        {
            _reward = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="action">
        /// SpeedUp 0  -
        /// SpeedUnchanged 1 -
        /// SpeedDown 2 -
        /// SpeedZero 3
        /// <para>&#160;</para> 
        /// RotationLeftStrong 4 -
        /// RotationLeft 5 - 
        /// RotationZero 6 -
        /// RotationRightStrong 7 -
        /// RotationRight 8</param>
        /// <param name="speed"></param>
        /// <param name="wallHit"></param>
        public static void GetReward(double speed, bool wallHit)
        {
                        //Log.Error("Report variabili: speed: " + speed + " action: " + action + " penalties: " + _penalties + " distanza goal: " + _distanceFromGoal + " ultima distanza: " + _lastDistanceFromGoal + " Reward: " + _reward);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="action">
        /// SpeedUp 0  -
        /// SpeedUnchanged 1 -
        /// SpeedDown 2 -
        /// SpeedZero 3
        /// </param>
        public static void speedReward(int action, double speed)
        {
            
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="action">
        /// RotationLeftStrong 4 -
        /// RotationLeft 5 - 
        /// RotationZero 6 -
        /// RotationRightStrong 7 -
        /// RotationRight 8
        /// </param>
        /// <param name="deltaDistance"></param>
        public static void rotationReward(int action, double deltaDistance)
        {
            
        }
        public static int readExistingTable(string path = @"C:\Addon Projects\TestFile\qTable.xlsx")
        {
           

        }
        public static int GetAction(AgentValues ag)
        {

            
        }

        public static int Train(AgentValues ag)
        {

            
        }

        public static int getLastAction()
        {
            return actionsBuffer.Last();
        }
        public static int getFirstAction()
        {
            return actionsBuffer.First();
        }
        public static bool repetitionCheck()
        {
            return actionsBuffer.Count != actionsBuffer.Distinct().Count();
        }

        /// SpeedUp 0  -
        /// SpeedUnchanged 1 -
        /// SpeedDown 2 -
        /// SpeedZero 3
        /// <para>&#160;</para> 
        /// RotationLeftStrong 4 -
        /// RotationLeft 5 - 
        /// RotationZero 6 -
        /// RotationRightStrong 7 -
        /// RotationRight 8
        public static bool anomalyCheck()
        {
            
        }
        public static int GetCurrentState(int x, int z)
        {
            
        }
        private static int counter = 0;


        public static void ExportToExcel()
        {

            
        }
    }
}