﻿// RL Project
// Ditolve Davide 806953
// Universita' degli studi Milano Bicocca
using RLDAL;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Common;
using RLDataLayer;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using Random = System.Random;
using UnityEngine.SceneManagement;
using log4net;
using UnityEngine.Profiling;

namespace Assets.Scripts
{

    /// <summary>
    /// Use this class to deal with FIXED UPDATE
    /// This class manage the agent with RL on unity.
    /// </summary>
    public class AgentController : MonoBehaviour
    {
        /// <summary>
        /// Logger for DB
        /// </summary>
        private static readonly ILog Log = CommonFeature.GetLogger("AgentController");
        /// <summary>
        /// Hidden gameobject to clone
        /// </summary>
        public GameObject ToReset;
        /// <summary>
        /// Used only for wallHit
        /// TODO Remove it
        /// </summary>
        private float _angle;
        /// <summary>
        /// Number of hit rays
        /// </summary>
        public int Rays = 1;
        /// <summary>
        /// Actual goal distance
        /// </summary>
        public float GoalDistance;
        /// <summary>
        /// Max hit distance
        /// </summary>
        public float MaxDistance;
        /// <summary>
        /// Walking speed
        /// </summary>
        public double WalkingSpeed;
        /// <summary>
        /// Agent ID from simulation manager
        /// </summary>
        private int _agentId;
        /// <summary>
        /// Current epoch
        /// </summary>
        private static int _epoch = 0;
        /// <summary>
        /// Current episode
        /// </summary>
        private int _episode = 0;
        /// <summary>
        /// Last decision from QLearning
        /// </summary>
        private int _lastDecision;
        [SerializeField] private GameObject _basement;

        #region DecisionVariable
        /// <summary>
        /// Back - Stay - Forward - Emergency break
        /// </summary>
        private int _speedDirection;
        /// <summary>
        /// Left - Right w strong, stay
        /// </summary>
        private int _rotationDirection;
        /// <summary>
        /// Delta in case forward or backward
        /// </summary>
        private double speedDelta = 10;
        /// <summary>
        /// Strong rotation
        /// </summary>
        private double rotationDeltaStrong = 10;
        /// <summary>
        /// Normal rotation
        /// </summary>
        private double rotationDelta = 5;
        /// <summary>
        /// Flag
        /// </summary>
        private double speedAmount = 0;
        /// <summary>
        /// Flag
        /// </summary>
        private double rotationAmount = 0;

        private double unstuckRotation = 90;

        #endregion DecisionVariable

        private double _lastReward;
        private double _epsilon;
        private bool _stop = false;

        private bool _wallHit = false;
        private Config _configuration { get; set; }

        private static int offsetX { get; set; }
        private static int offsetZ { get; set; }
        /// <summary>
        /// Setup envirorment variables and register first agent
        /// </summary>
        /// 
        private List<String> _checkpointReached = new List<string>();
        private int checkpointReached = 0;
        private void Start()
        {
        }
        /// <summary>
        /// Here is the call sequence, no bussiness logic will be written there.
        /// </summary>
        private void Update()
        {
            ManageDecision();
            Walk();
            // ManageRayCast();
        }

        /// <summary>
        /// This method is called with a frequency defined on DB
        /// Call the QLearning class and give it some parameters.
        /// </summary>
        public void FixedUpdate()
        {
            
        }

        /// <summary>
        /// Move to scene controller
        /// Method used to write stuff on camera overlay
        /// </summary>
        private void OnGUI()
        {
            
        }
        /// <summary>
        /// This method prepare all the agent values for DB.
        /// </summary>
        /// <returns>Data wrote on DB</returns>
        public AgentValues DBValues()
        {
           
        }

        /// <summary>
        /// Define what to do with every decision
        /// </summary>
        public void ManageDecision()
        {
            
        }
        /// <summary>
        /// This method manage perception from the envirorment
        /// </summary>
        public void ManageRayCast()
        {
            
        }
        private void OnCollisionEnter(Collision collision)
        {
            if(collision.gameObject.layer == LayerMask.NameToLayer("Checkpoint"))
            {
                _checkpointReached.Add(collision.gameObject.name);
            }
            else if (collision.gameObject.layer == LayerMask.NameToLayer("Muri"))
            {
                _wallHit = true;
                transform.position.Set(transform.position.x - 5, transform.position.y, transform.position.z - 5);
            }
            else if (collision.gameObject.layer == LayerMask.NameToLayer("Ostacoli"))
            {
                QLearning.obstacleHit = true;
            }
            Debug.Log("Collisoine");
        }

        private void OnCollisionExit(Collision collision)
        {
            if (collision.gameObject.layer == LayerMask.NameToLayer("Muri"))
            {
                _wallHit = false;
            }
            else if (collision.gameObject.layer == LayerMask.NameToLayer("Ostacoli"))
            {
                QLearning.obstacleHit = false;
            }
            Debug.Log("non più");
        }
        /// <summary>
        /// This method is called when 
        /// </summary>
        private void ResetSimulation()
        {
           
        }

        /// <summary>
        /// Setter for epoch variable
        /// </summary>
        /// <param name="epoch">Epoch</param>
        public static void setEpoch(int epoch)
        {
            _epoch = epoch;
        }
        /// <summary>
        /// Manage rotation variation
        /// </summary>
        public void SetRotation()
        {
            if (_rotationDirection == ROTATIONLEFT || _rotationDirection == ROTATIONRIGHT)
            {
                if (rotationAmount > (_wallHit? rotationDelta*5:rotationDelta))
                {
                    _stop = true;
                    rotationAmount = 0;
                    _rotationDirection = 0;
                }
            }
            if (_rotationDirection == ROTATIONLEFTSTRONG || _rotationDirection == ROTATIONRIGHTSTRONG)
            {
                if (rotationAmount > (_wallHit ? rotationDeltaStrong * 5 : rotationDeltaStrong))
                {
                    _stop = true;
                    rotationAmount = 0;
                    _rotationDirection = 0;
                }
            }

            if (!_stop && _rotationDirection != ROTATIONZERO)
            {
                transform.Rotate(0, 1 * (_wallHit?3:1) * _rotationDirection, 0);
            }

            rotationAmount += 1 * Math.Abs(_rotationDirection);
        }

        /// <summary>
        /// Manage every speed variation
        /// </summary>
        public void setWalkingSpeed()
        {
            //Emergency brake
            if (_speedDirection == SPEEDZERO)
            {
                var tmp = WalkingSpeed / 10;
                WalkingSpeed -= tmp;
            }
            //Slow down
            else if (_speedDirection < SPEEDUNCHANGED)
            {
                //if (WalkingSpeed >= 0)
                WalkingSpeed -= 0.5;
                if (speedAmount != speedDelta)
                {
                    speedAmount += 0.5;
                }
                else
                {
                    speedAmount = 0;
                    _speedDirection = 0;
                }
            }
            //Speed up
            else if (_speedDirection > SPEEDUNCHANGED)
            {
                WalkingSpeed += 0.5;
                if (speedAmount != speedDelta)
                {
                    speedAmount += 0.5;
                }
                else
                {
                    speedAmount = 0;
                    _speedDirection = 0;
                }
            }
            if (WalkingSpeed <= 0)
            {
                WalkingSpeed = 0; //To avoid negative speed
                _speedDirection = 0;
                speedAmount = speedDelta;
            }
        }
        /// <summary>
        /// Take screen shot from active camera
        /// </summary>
        /// <param name="idSession">Folder</param>
        /// <param name="episode">Name</param>
        /// <param name="epoch">Name</param>
        /// <code>
        /// ScreenCapture.CaptureScreenshot(@"C:\test\images\"+idSession+@"\"+epoch+"-"+episode+".png");
        /// </code>
        public void takeScreenshot(int idSession, int episode, int epoch)
        {
            
        }

        /// <summary>
        /// This function is always called to adjust speed every frame
        /// Since speed is calculated with sin and cos, every rotation will affect the final speed
        /// </summary>
        public void Walk()
        {
           
        }

        /// <summary>
        /// This method is no longer needed
        /// </summary>
        /// <param name="hit">Which gameobject has been hit</param>
        public void WallHit(GameObject hit)
        {
            //TODO MANAGE HIT
            transform.Rotate(0, _angle, 0);
        }

        // OnTriggerExit is called when the Collider other has stopped touching the trigger
        private void OnTriggerExit(Collider other)
        {
            if(other.gameObject.layer == LayerMask.NameToLayer("Walkable"))
            {
                QLearning.OutOfPath = true;
                Debug.Log("Out of correct path");
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("Walkable"))
            {
                QLearning.OutOfPath = false;
                Debug.Log("Correct path");
            }
            else if (other.gameObject.layer == LayerMask.NameToLayer("Goal"))
            {
                QLearning.GoalEpisode = _configuration.maxEpisode - _episode;
                QLearning.GoalReached = true;
                Debug.Log("Correct path");
            }
            else if (other.gameObject.layer == LayerMask.NameToLayer("AIAgents"))
            {
                Debug.Log("Other agent hit");
            }
            
        }
    }
}