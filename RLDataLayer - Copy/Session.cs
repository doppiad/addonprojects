// RL Project
// Ditolve Davide 806953
// Universita' degli studi Milano Bicocca
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace RLDataLayer
{
    [Table("Session")]
    public partial class Session
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Session()
        {
            Agents = new HashSet<Agent>();
        }

        public int ID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Agent> Agents { get; set; }

        [MaxLength(50)]
        public string Name { get; set; }

        public int IDConfig { get; set; }
        [ForeignKey("IDConfig")]
        public virtual Config Config { get; set; }

        public Session(int iD, ICollection<Agent> agent, string name, int iDConfig, Config config)
        {
            ID = iD;

        }

        public override string ToString()
        {
            return $"{{{nameof(ID)}={ID.ToString()}, {nameof(Agent)}={Agents}, {nameof(Name)}={Name}, {nameof(IDConfig)}={IDConfig.ToString()}, {nameof(Config)}={Config}}}";
        }
        public static List<String> VariablesList()
        {
            return typeof(Session)
                .GetFields()
                .Select(field => field.Name)
                .ToList();
        }
    }
}