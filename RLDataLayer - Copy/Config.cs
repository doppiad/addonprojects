// RL Project
// Ditolve Davide 806953
// Universita' degli studi Milano Bicocca

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
namespace RLDataLayer
{
    [Table("Config")]
    public partial class Config
    {
        public int ID { get; set; }

        public int RaysNumber { get; set; }
        [Range(0.0, Double.MaxValue)]
        public double maxDistance { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        public double WallRotationAngle { get; set; }
        [Range(0.0, 12.0)]
        public double maxWalkingSpeed { get; set; }
        [Range(0.0, 12.0)]
        public double minWalkingSpeed { get; set; }
        public int decisionPerMinute { get; set; }
        public int  maxEpisode { get; set; }
        public int maxEpoch { get; set; }

        public double speedDelta { get; set; }
        public double rotationDeltaStrong { get; set; }
        public double rotationDelta { get; set; }
        public int tileSize { get; set; }

        public double Alpha { get; set; }
        public double Gamma { get; set; }
        public ICollection<Session> Sessions { get; set; }

        public static List<String> VariablesList()
        {
            return typeof(Config)
                .GetFields()
                .Select(field => field.Name)
                .ToList();
        }

        public override string ToString() => base.ToString();

        public Config(int iD, int raysNumber, double maxDistance, string name, double wallRotationAngle, double maxWalkingSpeed, double minWalkingSpeed, int decisionPerMinute, int maxEpisode, int maxEpoch, double speedDelta, double rotationDeltaStrong, double rotationDelta, int tileSize, double alpha, double gamma)
        {
            ID = iD;
            RaysNumber = raysNumber;
            this.maxDistance = maxDistance;
            Name = name;
            WallRotationAngle = wallRotationAngle;
            this.maxWalkingSpeed = maxWalkingSpeed;
            this.minWalkingSpeed = minWalkingSpeed;
            this.decisionPerMinute = decisionPerMinute;
            this.maxEpisode = maxEpisode;
            this.maxEpoch = maxEpoch;
            this.speedDelta = speedDelta;
            this.rotationDeltaStrong = rotationDeltaStrong;
            this.rotationDelta = rotationDelta;
            this.tileSize = tileSize;
            Alpha = alpha;
            Gamma = gamma;
        }

        public Config()
        {
        }
    }
}