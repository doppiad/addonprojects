## Purpose of this file

In this file all profile analysis made on code will be commented

## Rays and collider analysis

As we can see in file *Collider.data* and *rays before collider* the cpu graph with collision detection instead of raycasting
has perfomance much more better and also lesser peaks.
Also collision detection allow us to gather more data.

## Version005 Analysis

This analysis shows how framerate slighty decrease from previous version. Rendering and animation have peaks, scripts execution
remains stable along all the life-cycle. The initial peak is consequence of loading process.

## Increasing Simulation Speed

Along the profiling, simulation speed has incresead constantly, as we can see, the higher is the simulation speed the higher is
the cpu needed to play the animation.
If you need to simulate in a more efficient way with higher speed, please remove the animation from Agente0 and his clones.