## Test File

The purpose of this folder is only for debugging and understanding better what's happening in the simulator.
You can also use this files to understand how the system has evolved.

Don't put files in this folder manually.