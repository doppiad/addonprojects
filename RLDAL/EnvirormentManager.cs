﻿// RL Project
// Ditolve Davide 806953
// Universita' degli studi Milano Bicocca
using RLDataLayer;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RLDAL
{
    /// <summary>
    /// This class has been design to hold all methods to manage the communication
    /// between unity simulator and DB, in particular this class has the logic for Config
    /// Config hold all the parameters for a single simulation
    /// </summary>
    public class EnvirormentManager
    {
        /// <summary>
        /// Every single configuration is identified by a numerical ID unknown to final user
        /// and a name. It can be chosen from main menu
        /// </summary>
        /// <param name="Name">Configuration name</param>
        /// <returns></returns>
        public static void createOrEditEnvirorment(RLDataLayer.Environment e)
        {
            using (var db = new RLDatabase())
            {
                if (db.Environments.Any(x => x.Name == e.Name))
                {
                    var toEdit = db.Environments.FirstOrDefault(x => x.Name == e.Name);
                    toEdit.columns = e.columns;
                    toEdit.offSetX = e.offSetX;
                    toEdit.offSetZ = e.offSetZ;
                    toEdit.rows = e.rows;
                    db.Entry(toEdit).State = EntityState.Modified;
                }
                else
                    db.Environments.Add(e);
                db.SaveChanges();
            }
        }
       
    }
}