﻿// RL Project
// Ditolve Davide 806953
// Universita' degli studi Milano Bicocca
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RLDataLayer;

namespace RLDAL
{
    /// <summary>
    /// This class has been design to hold all methods to manage the communication
    /// between unity simulator and DB, in particular this class has the logic for Simulation
    /// </summary>
    public class SimulationManager
    {
        /// <summary>
        /// Generate on DB new session to link with agents
        /// </summary>
        /// <param name="s">Session</param>
        /// <returns>DB-generated simulation ID</returns>
        /// 

        private static int _sessionID;

        public static int SessionID { get => _sessionID; set => _sessionID = value; }

        public static int CreateNewSimulation(Session s, int idConfig = 2)
        {
            using (var db = new RLDatabase())
            {
                s.IDConfig = idConfig;
                var toReturn = db.Session.Add(s);
                db.SaveChanges();
                SessionID = toReturn.ID;
                return toReturn.ID;
            }
        }
        /// <summary>
        /// Set session name equals to the scene loaded
        /// </summary>
        /// <param name="name">Scene name</param>
        public static void ChangeSimulationName(string name)
        {
            using (var db = new RLDatabase())
            {
                var toEdit = db.Session.FirstOrDefault(x => x.ID == SessionID);
                toEdit.Name = name;
                db.Entry(toEdit).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }
        public static void ChangeSimulationConfig(int config)
        {
            using (var db = new RLDatabase())
            {
                var toEdit = db.Session.FirstOrDefault(x => x.ID == SessionID);
                toEdit.IDConfig = config;
                db.Entry(toEdit).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }
        //TODO further development
    }
}