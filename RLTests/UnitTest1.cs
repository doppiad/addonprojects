﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RLTests
{
    [TestClass]
    public class GeneralTest
    {
        FixedSizedQueue<int> myList;
        [TestMethod]
        public void LinkedListTest()
        {
            myList = new FixedSizedQueue<int>(3);
            myList.Enqueue(1);
            myList.Enqueue(2);
            myList.Enqueue(3);
            Console.WriteLine(myList.First());
            myList.Enqueue(4);
            Console.WriteLine(myList.First());
        }
        public void print()
        {
            foreach (var q in myList)
            {
                Console.WriteLine(q);
            }
        }

        public class FixedSizedQueue<T> : ConcurrentQueue<T>
        {
            private readonly object syncObject = new object();

            public int Size { get; private set; }

            public FixedSizedQueue(int size)
            {
                Size = size;
            }

            public new void Enqueue(T obj)
            {
                base.Enqueue(obj);
                lock (syncObject)
                {
                    while (base.Count > Size)
                    {
                        T outObj;
                        base.TryDequeue(out outObj);
                    }
                }
            }
        }
    }
}
