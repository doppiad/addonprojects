## WELCOME

Welcome to my master thesis in IT SCIENCE @Unimib
I'm Davide Ditolve and my project aim to study pedestrian behaviours in various situations

Here you can find the [Changelog](https://gitlab.com/doppiad/addonprojects/-/blob/master/CHANGELOG.md)

## TODO
- heatmap da finire
- dati sui checkpoint
- dati su curva
- multi agente
- agenti scriptati x
- uscita più stretta e spostata
- colonnine come ostacoli
- mini buffer azioni

## Folders
In this repository you'll find lot a folders, in the graph below are shown only the folders with code and their relationship

```mermaid
graph TD

A["🎮" Unity Project RL]

B["🌐" Web Project]

A-->C["♲" Common]

B-->C

A-->D["🔗" PyConnector]

B-->D

A-->E["🤝" RLDAL]

B-->E

E-->F["💽" RLDataLayer]
```

## Unity Project RL
This folders contains all the files you need for [Unity](https://unity.com/)
## Web Project
This projects is C# .net framework for visualize all data gathered  through unity.
More information will be available soon
## Common
This project contains all method and class used from every project in order to avoid code rewriting
## PyConnector
Actually is not used. This class will help to load python script inside C# context (you can use it with webproject or unity project)
## RLDAL
This is the data access layer project used to access DB
## RLDataLayer
This project contains the definition of DB

## General information
Every single folder will contains his own readme
