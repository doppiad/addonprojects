USE [RL]
GO

/****** Object:  Table [dbo].[Envirorment]    Script Date: 08/05/2020 00:37:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Envirorment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[rows] [int] NOT NULL,
	[columns] [int] NOT NULL,
	[offSetX] [int] NOT NULL,
	[offSetY] [int] NOT NULL,
 CONSTRAINT [PK_Envirorment] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

