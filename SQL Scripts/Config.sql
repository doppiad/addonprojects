USE [RL]
GO

/****** Object:  Table [dbo].[Config]    Script Date: 08/05/2020 00:37:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Config](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RaysNumber] [int] NOT NULL,
	[maxDistance] [float] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[WallRotationAngle] [float] NOT NULL,
	[maxWalkingSpeed] [float] NOT NULL,
	[minWalkingSpeed] [float] NOT NULL,
	[decisionPerMinute] [int] NOT NULL,
	[maxEpisode] [int] NOT NULL,
	[maxEpoch] [int] NULL,
	[speedDelta] [float] NOT NULL,
	[rotationDeltaStrong] [float] NOT NULL,
	[rotationDelta] [float] NOT NULL,
	[tileSize] [int] NOT NULL,
	[Alpha] [float] NOT NULL,
	[Gamma] [float] NOT NULL,
 CONSTRAINT [PK_Config] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Config] ADD  CONSTRAINT [DF_Config_WallRotationAngle]  DEFAULT ((1)) FOR [WallRotationAngle]
GO

ALTER TABLE [dbo].[Config] ADD  CONSTRAINT [DF_Config_maxWalkingSpeed]  DEFAULT ((1.4)) FOR [maxWalkingSpeed]
GO

ALTER TABLE [dbo].[Config] ADD  CONSTRAINT [DF_Config_minWalkingSpeed]  DEFAULT ((0.8)) FOR [minWalkingSpeed]
GO

ALTER TABLE [dbo].[Config] ADD  CONSTRAINT [DF_Config_decisionPerMinute]  DEFAULT ((4)) FOR [decisionPerMinute]
GO

ALTER TABLE [dbo].[Config] ADD  CONSTRAINT [DF_Config_maxEpisode]  DEFAULT ((50)) FOR [maxEpisode]
GO

ALTER TABLE [dbo].[Config] ADD  CONSTRAINT [DF_Config_speedDelta]  DEFAULT ((10)) FOR [speedDelta]
GO

ALTER TABLE [dbo].[Config] ADD  CONSTRAINT [DF_Config_rotationDeltaStrong]  DEFAULT ((10)) FOR [rotationDeltaStrong]
GO

ALTER TABLE [dbo].[Config] ADD  CONSTRAINT [DF_Config_rotationDelta]  DEFAULT ((5)) FOR [rotationDelta]
GO

ALTER TABLE [dbo].[Config] ADD  CONSTRAINT [DF_Config_tileSize]  DEFAULT ((50)) FOR [tileSize]
GO

ALTER TABLE [dbo].[Config] ADD  CONSTRAINT [DF_Config_Alpha]  DEFAULT ((0.3)) FOR [Alpha]
GO

ALTER TABLE [dbo].[Config] ADD  CONSTRAINT [DF_Config_Gamma]  DEFAULT ((0.6)) FOR [Gamma]
GO

ALTER TABLE [dbo].[Config]  WITH CHECK ADD  CONSTRAINT [CK_Config] CHECK  (([decisionPerMinute]<(100)))
GO

ALTER TABLE [dbo].[Config] CHECK CONSTRAINT [CK_Config]
GO

