USE [RL]
GO

/****** Object:  Table [dbo].[AgentValues]    Script Date: 28/04/2020 01:32:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AgentValues](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_Alias] [int] NOT NULL,
	[vGoalX] [float] NOT NULL,
	[dGoal] [float] NOT NULL,
	[timestamp] [datetime] NOT NULL,
	[vGoalY] [float] NOT NULL,
	[vGoalZ] [float] NOT NULL,
	[Epoch] [int] NOT NULL,
	[Episode] [int] NOT NULL,
	[xPosition] [int] NOT NULL,
	[zPosition] [int] NOT NULL,
	[lastDecision] [int] NOT NULL,
	[speed] [float] NOT NULL,
	[simulationSpeed] [float] NULL,
	[cumulativeReward] [float] NULL,
 CONSTRAINT [PK_AgentValues] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AgentValues] ADD  CONSTRAINT [DF_AgentValues_Epoch]  DEFAULT ((0)) FOR [Epoch]
GO

ALTER TABLE [dbo].[AgentValues] ADD  CONSTRAINT [DF_AgentValues_Episode]  DEFAULT ((0)) FOR [Episode]
GO

ALTER TABLE [dbo].[AgentValues] ADD  CONSTRAINT [DF_AgentValues_xPosition]  DEFAULT ((0)) FOR [xPosition]
GO

ALTER TABLE [dbo].[AgentValues] ADD  CONSTRAINT [DF_AgentValues_yPosition]  DEFAULT ((0)) FOR [zPosition]
GO

ALTER TABLE [dbo].[AgentValues] ADD  CONSTRAINT [DF_AgentValues_lastDecision]  DEFAULT ((-1)) FOR [lastDecision]
GO

ALTER TABLE [dbo].[AgentValues] ADD  CONSTRAINT [DF_AgentValues_speed]  DEFAULT ((0)) FOR [speed]
GO

ALTER TABLE [dbo].[AgentValues]  WITH CHECK ADD  CONSTRAINT [Identified] FOREIGN KEY([ID_Alias])
REFERENCES [dbo].[Agent] ([ID])
GO

ALTER TABLE [dbo].[AgentValues] CHECK CONSTRAINT [Identified]
GO

