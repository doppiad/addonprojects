USE [RL]
GO

/****** Object:  Table [dbo].[AgentRaysValues]    Script Date: 28/04/2020 01:32:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AgentRaysValues](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_Ray] [int] NOT NULL,
	[ID_Alias] [int] NOT NULL,
	[dHit] [float] NOT NULL,
	[hitCount] [int] NOT NULL,
	[vHit] [float] NOT NULL,
 CONSTRAINT [PK_AgentRaysValues] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AgentRaysValues]  WITH CHECK ADD  CONSTRAINT [Featured] FOREIGN KEY([ID_Alias])
REFERENCES [dbo].[Agent] ([ID])
GO

ALTER TABLE [dbo].[AgentRaysValues] CHECK CONSTRAINT [Featured]
GO

ALTER TABLE [dbo].[AgentRaysValues]  WITH CHECK ADD  CONSTRAINT [CK_AgentRaysValues] CHECK  (([ID_Ray]>=(0)))
GO

ALTER TABLE [dbo].[AgentRaysValues] CHECK CONSTRAINT [CK_AgentRaysValues]
GO

