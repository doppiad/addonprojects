using System;
using System.Collections.Generic;
using System.Linq;

namespace RLDataLayer
{
    public partial class AgentRaysValues
    {
        public int ID { get; set; }

        public int ID_Ray { get; set; }

        public int ID_Alias { get; set; }

        public double dHit { get; set; }

        public int hitCount { get; set; }

        public double vHit { get; set; }

        public virtual Agent Agent { get; set; }

        public override string ToString()
        {
            return $"{{{nameof(ID)}={ID.ToString()}, {nameof(ID_Ray)}={ID_Ray.ToString()}, {nameof(ID_Alias)}={ID_Alias.ToString()}, {nameof(dHit)}={dHit.ToString()}, {nameof(hitCount)}={hitCount.ToString()}, {nameof(vHit)}={vHit.ToString()}, {nameof(Agent)}={Agent}}}";
        }

        public AgentRaysValues(int iD, int iD_Ray, int iD_Alias, double dHit, int hitCount, double vHit, Agent agent)
        {
            ID = iD;
            ID_Ray = iD_Ray;
            ID_Alias = iD_Alias;
            this.dHit = dHit;
            this.hitCount = hitCount;
            this.vHit = vHit;
            Agent = agent ?? throw new ArgumentNullException(nameof(agent));
        }
        public static List<String> VariablesList()
        {
            return typeof(AgentRaysValues)
                .GetFields()
                .Select(field => field.Name)
                .ToList();
        }
    }
}
