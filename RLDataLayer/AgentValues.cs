// RL Project
// Ditolve Davide 806953
// Universita' degli studi Milano Bicocca
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
namespace RLDataLayer
{
    public partial class Checkpoint : AgentValues { }
    public partial class AgentValues
    {
        public int ID { get; set; }

        public int ID_Alias { get; set; }

        [Required]
        public double vGoalX { get; set; }

        [Required]
        public double vGoalY { get; set; }

        [Required]
        public double vGoalZ { get; set; }

        public double dGoal { get; set; }

        public DateTime timestamp { get; set; }

        public virtual Agent Agent { get; set; }
        public int Epoch { get; set; }
        public int Episode { get; set; }
        public int xPosition { get; set; }
        public int zPosition { get; set; }
        public int lastDecision { get; set; }
        public double speed { get; set; }
        public double simulationSpeed { get; set; }
        public double? cumulativeReward { get; set; }
        public bool wallHit { get; set; }
        public override string ToString()
        {
            return base.ToString();
        }

        public AgentValues()
        {
        }

        public AgentValues(int iD, int iD_Alias, double vGoalX, double vGoalY, double vGoalZ, double dGoal, DateTime timestamp, Agent agent, int epoch, int episode, int xPosition, int zPosition, int lastDecision, double speed, double simulationSpeed, double? cumulativeReward,bool wallHit)
        {
            ID = iD;
            ID_Alias = iD_Alias;
            this.vGoalX = vGoalX;
            this.vGoalY = vGoalY;
            this.vGoalZ = vGoalZ;
            this.dGoal = dGoal;
            this.timestamp = timestamp;
            Agent = agent ?? throw new ArgumentNullException(nameof(agent));
            Epoch = epoch;
            Episode = episode;
            this.xPosition = xPosition;
            this.zPosition = zPosition;
            this.lastDecision = lastDecision;
            this.speed = speed;
            this.simulationSpeed = simulationSpeed;
            this.cumulativeReward = cumulativeReward;
            this.wallHit = wallHit;
        }
        public static List<String> VariablesList()
        {
            return typeof(AgentValues)
                .GetFields()
                .Select(field => field.Name)
                .ToList();
        }
    }
}