// RL Project
// Ditolve Davide 806953
// Universita' degli studi Milano Bicocca
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace RLDataLayer
{
    [Table("Agent")]
    public partial class Agent
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Agent()
        {
            AgentRaysValues = new HashSet<AgentRaysValues>();
            AgentValues = new HashSet<AgentValues>();
        }

        public int ID { get; set; }

        public int ID_Session { get; set; }

        [Required]
        [StringLength(255)]
        public string Alias { get; set; }

        public virtual Session Session { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AgentRaysValues> AgentRaysValues { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AgentValues> AgentValues { get; set; }

        public Agent(int iD, int iD_Session, string alias, Session session, ICollection<AgentRaysValues> agentRaysValues, ICollection<AgentValues> agentValues)
        {
            ID = iD;
            ID_Session = iD_Session;
            Alias = alias ?? throw new ArgumentNullException(nameof(alias));
            Session = session ?? throw new ArgumentNullException(nameof(session));
            AgentRaysValues = agentRaysValues ?? throw new ArgumentNullException(nameof(agentRaysValues));
            AgentValues = agentValues ?? throw new ArgumentNullException(nameof(agentValues));
        }

        public override string ToString()
        {
            return $"{{{nameof(ID)}={ID.ToString()}, {nameof(ID_Session)}={ID_Session.ToString()}, {nameof(Alias)}={Alias}, {nameof(Session)}={Session}, {nameof(AgentRaysValues)}={AgentRaysValues}, {nameof(AgentValues)}={AgentValues}}}";
        }
        public static List<String> VariablesList()
        {
            return typeof(Agent)
                .GetFields()
                .Select(field => field.Name)
                .ToList();
        }
    }
}