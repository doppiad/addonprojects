// RL Project
// Ditolve Davide 806953
// Universita' degli studi Milano Bicocca
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using System.Threading.Tasks;

namespace RLDataLayer
{
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
    public partial class RLDatabase : IdentityDbContext<ApplicationUser>

    {
        public RLDatabase()
            : base("data source=DESKTOP-PDEA21V;initial catalog=RL4;User ID=sa;Password=RLPassword;App=EntityFramework;") //base("data source=progettorl.database.windows.net;Initial Catalog=RL;Persist Security Info=False;User ID=davide;Password=Wiicfck8uz!;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;") ////base("data source=progettorl.database.windows.net;Initial Catalog=RL;Persist Security Info=False;User ID=davide;Password=Wiicfck8uz!;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;") 
        {
        }

        public virtual DbSet<Agent> Agents { get; set; }
        public virtual DbSet<AgentRaysValues> AgentRaysValues { get; set; }
        public virtual DbSet<AgentValues> AgentValues { get; set; }
        public virtual DbSet<Config> Config { get; set; }
        public virtual DbSet<Session> Session { get; set; }
        public virtual DbSet<Environment> Environments { get; set; }
        public virtual DbSet<Checkpoint> GetCheckpoints { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Agent>()
                .HasMany(e => e.AgentRaysValues)
                .WithRequired(e => e.Agent)
                .HasForeignKey(e => e.ID_Alias)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Agent>()
                .HasMany(e => e.AgentValues)
                .WithRequired(e => e.Agent)
                .HasForeignKey(e => e.ID_Alias)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Config>()
                .HasMany(e => e.Sessions)
                .WithRequired(e => e.Config)
                .HasForeignKey(e => e.IDConfig)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Session>()
                .HasMany(e => e.Agents)
                .WithRequired(e => e.Session)
                .HasForeignKey(e => e.ID_Session);
        }
        public static RLDatabase Create()
        {
            return new RLDatabase();
        }
    }
}