﻿using Common;
using RLDAL;
using RLDataLayer;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

namespace Assets.Scripts
{
    public class AIAgent : MonoBehaviour
    {
        public GameObject WayPoints;
        // Start is called before the first frame update
        private List<Vector3> _waypoints = new List<Vector3>();
        private Vector3 _waypointChoosen;
        private NavMeshAgent nma;
        public double RemainingDistance;
        private Config _configuration { get; set; }
        double MaxSpeed, MinSpeed;
        void Start()
        {
            GetComponent<TrailRenderer>().enabled = false;
            GetComponent<TrailRenderer>().startColor = Color.white;
            GetComponent<TrailRenderer>().endColor = Color.red;
            GetComponent<TrailRenderer>().enabled = true;
            _configuration = ConfigManager.GetConfigurationByName(SceneManager.GetActiveScene().name);
            MaxSpeed = _configuration.maxWalkingSpeed;
            MinSpeed = _configuration.minWalkingSpeed;
            nma = GetComponent<NavMeshAgent>();
            for (int i = 0; i < WayPoints.transform.childCount; i++)
            {
                _waypoints.Add(WayPoints.transform.GetChild(i).position);
            }
            System.Random r = new System.Random();
            int index = r.Next(_waypoints.Count);
            _waypointChoosen = _waypoints[index];
            nma.destination = _waypointChoosen;
        }
        public void OnGUI()
        {

            if (GUI.Button(new Rect(10, 10, 100, 50), ConstantDefinition.BUTTONADDAI))
                Debug.Log("Clicked the button with text");
        }
        // Update is called once per frame
        void Update()
        {
            
        }
        private void FixedUpdate()
        {
            System.Random r = new System.Random();
            RemainingDistance = nma.remainingDistance;
            if (nma.remainingDistance < 30)
            {

                int index = r.Next(_waypoints.Count);
                _waypointChoosen = _waypoints[index];
                nma.destination = _waypointChoosen;
            }
            nma.speed =(float) CommonFeature.GetRandomNumber(MinSpeed, MaxSpeed) * 55;
        }
    }
}