﻿// RL Project
// Ditolve Davide 806953
// Universita' degli studi Milano Bicocca
using RLDAL;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Common;
using RLDataLayer;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using Random = System.Random;
using static RLDAL.AgentValuesManager;
using static Assets.Scripts.ConstantDefinition;
using UnityEngine.SceneManagement;
using log4net;
using UnityEngine.Profiling;

namespace Assets.Scripts
{

    /// <summary>
    /// Use this class to deal with FIXED UPDATE
    /// This class manage the agent with RL on unity.
    /// </summary>
    public class AgentController : MonoBehaviour
    {
        /// <summary>
        /// Logger for DB
        /// </summary>
        private static readonly ILog Log = CommonFeature.GetLogger("AgentController");
        /// <summary>
        /// Hidden gameobject to clone
        /// </summary>
        public GameObject ToReset;
        /// <summary>
        /// Used only for wallHit
        /// TODO Remove it
        /// </summary>
        private float _angle;
        /// <summary>
        /// Number of hit rays
        /// </summary>
        public int Rays = 1;
        /// <summary>
        /// Actual goal distance
        /// </summary>
        public float GoalDistance;
        /// <summary>
        /// Max hit distance
        /// </summary>
        public float MaxDistance;
        /// <summary>
        /// Walking speed
        /// </summary>
        public double WalkingSpeed;
        /// <summary>
        /// Agent ID from simulation manager
        /// </summary>
        private int _agentId;
        /// <summary>
        /// Current epoch
        /// </summary>
        private static int _epoch = 0;
        /// <summary>
        /// Current episode
        /// </summary>
        private int _episode = 0;
        /// <summary>
        /// Last decision from QLearning
        /// </summary>
        private int _lastDecision;
        [SerializeField] private GameObject _basement;

        #region DecisionVariable
        /// <summary>
        /// Back - Stay - Forward - Emergency break
        /// </summary>
        private int _speedDirection;
        /// <summary>
        /// Left - Right w strong, stay
        /// </summary>
        private int _rotationDirection;
        /// <summary>
        /// Delta in case forward or backward
        /// </summary>
        private double speedDelta = 10;
        /// <summary>
        /// Strong rotation
        /// </summary>
        private double rotationDeltaStrong = 10;
        /// <summary>
        /// Normal rotation
        /// </summary>
        private double rotationDelta = 5;
        /// <summary>
        /// Flag
        /// </summary>
        private double speedAmount = 0;
        /// <summary>
        /// Flag
        /// </summary>
        private double rotationAmount = 0;

        private double unstuckRotation = 90;

        #endregion DecisionVariable

        private double _lastReward;
        private double _epsilon;
        private bool _stop = false;

        private bool _wallHit = false;
        private Config _configuration { get; set; }

        private static int offsetX { get; set; }
        private static int offsetZ { get; set; }
        /// <summary>
        /// Setup envirorment variables and register first agent
        /// </summary>
        /// 
        private List<String> _checkpointReached = new List<string>();
        private int checkpointReached = 0;
        private void Start()
        {

            _angle = 0;
            _configuration = ConfigManager.GetConfigurationByName(SceneManager.GetActiveScene().name);
            Rays = _configuration.RaysNumber;
            MaxDistance = (float)_configuration.maxDistance;
            //MaxDistance = MaxDistance < 1 ? 1 : MaxDistance; Check no needed anymore. Changed are made by web interface
            _angle = (float)_configuration.WallRotationAngle;
            SimulationManager.ChangeSimulationName(SceneManager.GetActiveScene().name);
            SimulationManager.ChangeSimulationConfig(_configuration.ID);
            QLearning.MaxSpeed = _configuration.maxWalkingSpeed;
            QLearning.MinSpeed = _configuration.minWalkingSpeed;
            if (gameObject.name == "Agente0")
            {
                _basement.GetComponent<Grid>().cellSize = new Vector3(_configuration.tileSize, 1, _configuration.tileSize);

                _basement.GetComponent<HeatMap>().Rows = (int)(10 * _basement.GetComponent<RectTransform>().localScale.x) / _configuration.tileSize;
                _basement.GetComponent<HeatMap>().Columns = (int)(10 * _basement.GetComponent<RectTransform>().localScale.z) / _configuration.tileSize;
                _basement.GetComponent<HeatMap>().ZeroPosition = new Vector3(_basement.GetComponent<RectTransform>().anchoredPosition3D.x - 5 * _basement.GetComponent<RectTransform>().localScale.x, _basement.GetComponent<RectTransform>().anchoredPosition3D.y, _basement.GetComponent<RectTransform>().anchoredPosition3D.z - 5 * _basement.GetComponent<RectTransform>().localScale.z);
                _basement.GetComponent<HeatMap>().LastPosition = new Vector3(_basement.GetComponent<RectTransform>().anchoredPosition3D.x + 5 * _basement.GetComponent<RectTransform>().localScale.x, _basement.GetComponent<RectTransform>().anchoredPosition3D.y, _basement.GetComponent<RectTransform>().anchoredPosition3D.z + 5 * _basement.GetComponent<RectTransform>().localScale.z);
                _basement.GetComponent<HeatMap>().TileSize = _configuration.tileSize;
                QLearning.States = (int)((100 * _basement.GetComponent<RectTransform>().localScale.x * _basement.GetComponent<RectTransform>().localScale.z) / (_configuration.tileSize * _configuration.tileSize));  //= (int)Math.Ceiling((double)((double)25000000 / (double)2500));//25000000;
                QLearning.QTable = new double[QLearning.States, QLearning.Actions];
                QLearning.Epsilon = 3f / (float)_epoch;
                QLearning.Alpha = _configuration.Alpha;
                QLearning.Gamma = _configuration.Gamma;
                offsetX = (int)-_basement.GetComponent<Grid>().LocalToCell(_basement.GetComponent<HeatMap>().ZeroPosition).x;
                offsetZ = (int)-_basement.GetComponent<Grid>().LocalToCell(_basement.GetComponent<HeatMap>().ZeroPosition).z;
                QLearning.TilePerRow = _basement.GetComponent<HeatMap>().Columns;
                RLDataLayer.Environment e = new RLDataLayer.Environment
                {
                    columns = _basement.GetComponent<HeatMap>().Columns,
                    Name = SceneManager.GetActiveScene().name,
                    offSetX = offsetX,
                    offSetZ = offsetZ,
                    rows = _basement.GetComponent<HeatMap>().Rows
                };
                RLDAL.EnvirormentManager.createOrEditEnvirorment(e);
                gameObject.name = "Agente" + _epoch;
            }
            Agent a = new Agent()
            {
                Alias = Path.GetRandomFileName()
            };
            WalkingSpeed = CommonFeature.GetRandomNumber(ConfigManager.getSpeedLimit().Item1, ConfigManager.getSpeedLimit().Item2) * 55; // 55 ~1.37m/s 1 ~ 0.024909m/s
            _agentId = registerAgent(a);
        }
        /// <summary>
        /// Here is the call sequence, no bussiness logic will be written there.
        /// </summary>
        private void Update()
        {
            ManageDecision();
            Walk();
            // ManageRayCast();
        }

        /// <summary>
        /// This method is called with a frequency defined on DB
        /// Call the QLearning class and give it some parameters.
        /// </summary>
        public void FixedUpdate()
        {
            Profiler.BeginSample("WriteonDB");
            var av = DBValues();
            Profiler.EndSample();
            Profiler.BeginSample("Train");
            QLearning.Train(av);
            Profiler.EndSample();
            Profiler.BeginSample("RestOfCode");
            if (_checkpointReached.Count != checkpointReached)
            {
                QLearning.Checkpoint = true;
                checkpointReached++;
            }
            _lastDecision = QLearning.GetAction(av);
            _lastReward = QLearning.GetLastReward();
            _epsilon = QLearning.GetEpsilon();
            switch (_lastDecision)
            {
                case (int)DecisionList.SpeedUp:
                    _speedDirection = SPEEDUP;
                    break;

                case (int)DecisionList.RotationLeft:
                    _rotationDirection = ROTATIONLEFT;
                    _stop = false;
                    break;

                case (int)DecisionList.RotationLeftStrong:
                    _rotationDirection = ROTATIONLEFTSTRONG;
                    _stop = false;
                    break;

                case (int)DecisionList.RotationRight:
                    _rotationDirection = ROTATIONRIGHT;
                    _stop = false;
                    break;

                case (int)DecisionList.RotationRightStrong:
                    _rotationDirection = ROTATIONRIGHTSTRONG;
                    _stop = false;
                    break;

                case (int)DecisionList.RotationZero:
                    _rotationDirection = ROTATIONZERO;
                    _stop = false;
                    break;

                case (int)DecisionList.SpeedDown:
                    _speedDirection = SPEEDDOWN;
                    break;

                case (int)DecisionList.SpeedUnchanged:
                    _speedDirection = SPEEDUNCHANGED;
                    break;

                case (int)DecisionList.SpeedZero:
                    _speedDirection = SPEEDZERO;
                    break;
                default:
                    break;
            }
            QLearning.Checkpoint = false;
            if (_episode % 15 == 0)
            {
                takeScreenshot(SimulationManager.SessionID, _episode, _epoch);
            }
            if (_episode == _configuration.maxEpisode || QLearning.GoalReached)
                ResetSimulation();
            else
                _episode++;
            Profiler.EndSample();
        }

        /// <summary>
        /// Move to scene controller
        /// Method used to write stuff on camera overlay
        /// </summary>
        private void OnGUI()
        {
            GUIStyle fontSize = new GUIStyle(GUI.skin.GetStyle("label"))
            {
                fontSize = 18
            };
            GUI.Label(new Rect(100, 0, 200, 40), "Epsilon: " + _epsilon.ToString(), fontSize);
            GUI.Label(new Rect(100, 40, 200, 40), "Last Reward: " + QLearning.ValueToSave.ToString(), fontSize);
            GUI.Label(new Rect(100, 80, 200, 40), "Simulation speed: " + Time.timeScale, fontSize);
            GUI.Label(new Rect(100, 160, 200, 40), "Speed: " + (WalkingSpeed * 0.024909).ToString() + " m/s", fontSize);
            GUI.Label(new Rect(100, 200, 200, 40), "Choice: " + ((DecisionList)_lastDecision).ToString(), fontSize);
            GUI.Label(new Rect(100, 240, 200, 40), "Distance: " + (GoalDistance.ToString(), fontSize));
        }
        /// <summary>
        /// This method prepare all the agent values for DB.
        /// </summary>
        /// <returns>Data wrote on DB</returns>
        public AgentValues DBValues()
        {
            var goalSpeedRelativePointVelocity = gameObject.GetComponent<Rigidbody>()
                .GetRelativePointVelocity(GameObject.Find("Goal").transform.position);
            // Calculating distance and speed
            //GoalDistance = Vector3.Distance(GameObject.Find("Goal").transform.position, gameObject.transform.position);
            GoalDistance = (Vector3.Distance(GameObject.Find("Goal").GetComponent<BoxCollider>().ClosestPoint(transform.position), transform.position));
            Debug.DrawLine(transform.position, GameObject.Find("Goal").GetComponent<BoxCollider>().ClosestPoint(transform.position), Color.red);
            var cell = _basement.GetComponent<Grid>().LocalToCell(transform.position);

            AgentValues av = new AgentValues()
            {
                dGoal = Math.Round(GoalDistance,3,MidpointRounding.ToEven),
                vGoalX = Math.Round(goalSpeedRelativePointVelocity.x, 3, MidpointRounding.ToEven),
                vGoalY = Math.Round(goalSpeedRelativePointVelocity.y, 3, MidpointRounding.ToEven),
                vGoalZ = Math.Round(goalSpeedRelativePointVelocity.z, 3, MidpointRounding.ToEven),
                ID_Alias = _agentId,
                timestamp = DateTime.Now,
                Epoch = _epoch,
                Episode = _episode,
                xPosition = cell.x+offsetX,
                zPosition = cell.z+offsetZ,
                speed = Math.Round(WalkingSpeed * 0.024909, 4, MidpointRounding.ToEven), //Conversion factor between unity unit and m/s
                lastDecision = _lastDecision,
                simulationSpeed = Time.timeScale,
                cumulativeReward = QLearning.GetCumulativeReward(),
                wallHit = _wallHit
            };
            appendValuesToContext(av);
            return av;
        }

        /// <summary>
        /// Define what to do with every decision
        /// </summary>
        public void ManageDecision()
        {
            switch (_lastDecision)
            {
                case (int)DecisionList.SpeedUp:
                case (int)DecisionList.SpeedUnchanged:
                //case (int)DecisionList.SpeedZero:
                case (int)DecisionList.SpeedDown:
                    setWalkingSpeed();
                    break;

                case (int)DecisionList.RotationLeft:
                case (int)DecisionList.RotationLeftStrong:
                case (int)DecisionList.RotationRight:
                case (int)DecisionList.RotationRightStrong:
                case (int)DecisionList.RotationZero:
                    SetRotation();
                    break;
            }
        }
        /// <summary>
        /// This method manage perception from the envirorment
        /// </summary>
        public void ManageRayCast()
        {
            var layerMask = LayerMask.GetMask("Ostacoli", "Omini", "Muri");
            // Does the ray intersect any objects excluding the player layer
            var angle = 0f;
            for (int i = 0; i < Rays; i++)
            {
                angle += 2 * Mathf.PI / Rays;
                float xInput = Mathf.Sin(angle);
                float zInput = Mathf.Cos(angle);

                var transformPosition = transform.position;
                var directionalVector = new Vector3(xInput, 0, zInput);
                if (Physics.Raycast(transformPosition, transform.TransformDirection(directionalVector), out var hit,
                    MaxDistance, layerMask))
                {
                    Enum.TryParse(hit.transform.gameObject.tag, out hitType hittype);
                    if (hittype == hitType.Wall && i == Rays - 1) //Front hit on wall
                    {
                        _wallHit = true;
                        //WallHit(gameObject);
                    }

                    Debug.DrawRay(transform.position, transform.TransformDirection(directionalVector) * hit.distance,
                        i == Rays - 1 ? Color.blue : Color.red);
                    //Debug.Log("Did Hit");
                }
                else
                {
                    Debug.DrawRay(transformPosition, transform.TransformDirection(directionalVector) * MaxDistance,
                        i == Rays - 1 ? Color.black : Color.green);
                    //Debug.Log("Did not Hit");
                }
            }
        }
        private void OnCollisionEnter(Collision collision)
        {
            if(collision.gameObject.layer == LayerMask.NameToLayer("Checkpoint"))
            {
                _checkpointReached.Add(collision.gameObject.name);
            }
            else if (collision.gameObject.layer == LayerMask.NameToLayer("Muri"))
            {
                _wallHit = true;
                transform.position.Set(transform.position.x - 5, transform.position.y, transform.position.z - 5);
            }
            else if (collision.gameObject.layer == LayerMask.NameToLayer("Ostacoli"))
            {
                QLearning.obstacleHit = true;
            }
            Debug.Log("Collisoine");
        }

        private void OnCollisionExit(Collision collision)
        {
            if (collision.gameObject.layer == LayerMask.NameToLayer("Muri"))
            {
                _wallHit = false;
            }
            else if (collision.gameObject.layer == LayerMask.NameToLayer("Ostacoli"))
            {
                QLearning.obstacleHit = false;
            }
            Debug.Log("non più");
        }
        /// <summary>
        /// This method is called when 
        /// </summary>
        private void ResetSimulation()
        {
            try
            {
                QLearning.SetLastEpoch(_epoch);
                if (_epoch == _configuration.maxEpoch)
                    Application.Quit(REACHEDMAXEPOCH);
                _epoch++;
                var newObj = Instantiate(ToReset);
                newObj.GetComponent<AgentController>().ToReset = ToReset;
                newObj.SetActive(true);
                newObj.name = "Agente" + _epoch;
                //TODO FIX COLLISION WHILE IDLE
                //GameObject.Find("Agente" + (_epoch - 1)).GetComponent<Rigidbody>().velocity = Vector3.zero;
                //GameObject.Find("Agente" + (_epoch - 1)).GetComponent<AgentController>().enabled = false;
                Destroy(GameObject.Find("Agente" + (_epoch - 1)));
                QLearning.resetReward();
            }
            catch (Exception e)
            {
                Log.Error("Something went wrong resetting the simulation", e);
            }
        }

        /// <summary>
        /// Setter for epoch variable
        /// </summary>
        /// <param name="epoch">Epoch</param>
        public static void setEpoch(int epoch)
        {
            _epoch = epoch;
        }
        /// <summary>
        /// Manage rotation variation
        /// </summary>
        public void SetRotation()
        {
            if (_rotationDirection == ROTATIONLEFT || _rotationDirection == ROTATIONRIGHT)
            {
                if (rotationAmount > (_wallHit? rotationDelta*5:rotationDelta))
                {
                    _stop = true;
                    rotationAmount = 0;
                    _rotationDirection = 0;
                }
            }
            if (_rotationDirection == ROTATIONLEFTSTRONG || _rotationDirection == ROTATIONRIGHTSTRONG)
            {
                if (rotationAmount > (_wallHit ? rotationDeltaStrong * 5 : rotationDeltaStrong))
                {
                    _stop = true;
                    rotationAmount = 0;
                    _rotationDirection = 0;
                }
            }

            if (!_stop && _rotationDirection != ROTATIONZERO)
            {
                transform.Rotate(0, 1 * (_wallHit?3:1) * _rotationDirection, 0);
            }

            rotationAmount += 1 * Math.Abs(_rotationDirection);
        }

        /// <summary>
        /// Manage every speed variation
        /// </summary>
        public void setWalkingSpeed()
        {
            //Emergency brake
            if (_speedDirection == SPEEDZERO)
            {
                var tmp = WalkingSpeed / 10;
                WalkingSpeed -= tmp;
            }
            //Slow down
            else if (_speedDirection < SPEEDUNCHANGED)
            {
                //if (WalkingSpeed >= 0)
                WalkingSpeed -= 0.5;
                if (speedAmount != speedDelta)
                {
                    speedAmount += 0.5;
                }
                else
                {
                    speedAmount = 0;
                    _speedDirection = 0;
                }
            }
            //Speed up
            else if (_speedDirection > SPEEDUNCHANGED)
            {
                WalkingSpeed += 0.5;
                if (speedAmount != speedDelta)
                {
                    speedAmount += 0.5;
                }
                else
                {
                    speedAmount = 0;
                    _speedDirection = 0;
                }
            }
            if (WalkingSpeed <= 0)
            {
                WalkingSpeed = 0; //To avoid negative speed
                _speedDirection = 0;
                speedAmount = speedDelta;
            }
        }
        /// <summary>
        /// Take screen shot from active camera
        /// </summary>
        /// <param name="idSession">Folder</param>
        /// <param name="episode">Name</param>
        /// <param name="epoch">Name</param>
        /// <code>
        /// ScreenCapture.CaptureScreenshot(@"C:\test\images\"+idSession+@"\"+epoch+"-"+episode+".png");
        /// </code>
        public void takeScreenshot(int idSession, int episode, int epoch)
        {
            if (!Directory.Exists(@"C:\test\images\" + idSession + @"\"))
                Directory.CreateDirectory(@"C:\test\images\" + idSession + @"\");
            ScreenCapture.CaptureScreenshot(@"C:\test\images\" + idSession + @"\" + epoch + "-" + episode + ".png");
        }

        /// <summary>
        /// This function is always called to adjust speed every frame
        /// Since speed is calculated with sin and cos, every rotation will affect the final speed
        /// </summary>
        public void Walk()
        {
            gameObject.GetComponent<Rigidbody>().velocity = new Vector3(
                (float)(WalkingSpeed * Mathf.Sin((float)(transform.rotation.eulerAngles.y * Math.PI / 180))),
                0,
                (float)(WalkingSpeed * Mathf.Cos((float)(transform.rotation.eulerAngles.y * Math.PI / 180))));
        }

        /// <summary>
        /// This method is no longer needed
        /// </summary>
        /// <param name="hit">Which gameobject has been hit</param>
        public void WallHit(GameObject hit)
        {
            //TODO MANAGE HIT
            transform.Rotate(0, _angle, 0);
        }

        // OnTriggerExit is called when the Collider other has stopped touching the trigger
        private void OnTriggerExit(Collider other)
        {
            if(other.gameObject.layer == LayerMask.NameToLayer("Walkable"))
            {
                QLearning.OutOfPath = true;
                Debug.Log("Out of correct path");
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("Walkable"))
            {
                QLearning.OutOfPath = false;
                Debug.Log("Correct path");
            }
            else if (other.gameObject.layer == LayerMask.NameToLayer("Goal"))
            {
                QLearning.GoalEpisode = _configuration.maxEpisode - _episode;
                QLearning.GoalReached = true;
                Debug.Log("Correct path");
            }
            else if (other.gameObject.layer == LayerMask.NameToLayer("AIAgents"))
            {
                Debug.Log("Other agent hit");
            }
            
        }
    }
}