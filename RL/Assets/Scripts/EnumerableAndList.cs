﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public enum hitType
    {
        Wall,
        Person,
        Other
    }
    /// <summary>
    /// Possible decisions that could be taken by agents
    /// </summary>
    public enum DecisionList
    {
        SpeedUp,
        SpeedUnchanged,
        SpeedDown,
        SpeedZero,
        RotationLeftStrong,
        RotationLeft,
        RotationZero,
        RotationRightStrong,
        RotationRight,
    }
    public static class ConstantDefinition
    {
        #region costant definition
        public const int SPEEDUP = 1;
        public const int ROTATIONLEFT = -1;
        public const int ROTATIONLEFTSTRONG = -2;
        public const int ROTATIONRIGHT = 1;
        public const int ROTATIONRIGHTSTRONG = 2;
        public const int ROTATIONZERO = 0;
        public const int SPEEDDOWN = -1;
        public const int SPEEDUNCHANGED = 0;
        public const int SPEEDZERO = -999;
        public const int REACHEDMAXEPOCH = 100;
        public const string BUTTONADDAI = "ADD AI AGENT";
        #endregion
    }
}
