﻿// RL Project
// Ditolve Davide 806953
// Universita' degli studi Milano Bicocca
using RLDAL;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using RLDataLayer;
using UnityEngine;
using Random = System.Random;
using static RLDAL.AgentValuesManager;

namespace Assets.Scripts
{
    [Obsolete("This class doesn't use Fixed UPDATE, please refer to AgentController", false)]
    public class RayCastClient : MonoBehaviour
    {
        // Start is called before the first frame update
        public float yShift = 0;

        private float _maxY;
        private float _angle;
        public int Rays = 1;
        public float GoalDistance;
        public float Thickness;
        public float MaxDistance;
        public double walkingSpeed;
        private int _agentId;

        private void Start()
        {
            _maxY = GetComponentInChildren<CapsuleCollider>().height;
            _angle = 0;
            Thickness = 1;
            var config = ConfigManager.GetConfigurationByName("Test");
            Rays = config.RaysNumber;
            MaxDistance = (float)config.maxDistance;
            _angle = (float)config.WallRotationAngle;
            Random r = new Random();
            walkingSpeed = GetRandomNumber(config.minWalkingSpeed, config.maxWalkingSpeed);
            Agent a = new Agent()
            {
                Alias = Path.GetRandomFileName()
            };
            _agentId = registerAgent(a);
        }

        private void Update()
        {
            // Check variables
            // TO BE moved in Start, could be changed only via DB
            yShift = yShift > _maxY ? 2 : yShift;
            yShift = yShift < 0 ? 0 : yShift;
            MaxDistance = MaxDistance < 1 ? 1 : MaxDistance;
            gameObject.GetComponent<Rigidbody>().velocity = new Vector3(
                (float)(walkingSpeed * Mathf.Sin((float)(transform.rotation.eulerAngles.y * Math.PI / 180))),
                0,
                (float)(walkingSpeed * Mathf.Cos((float)(transform.rotation.eulerAngles.y * Math.PI / 180))));
            var goalSpeedRelativePointVelocity = gameObject.GetComponent<Rigidbody>().GetRelativePointVelocity(GameObject.Find("Goal").transform.position);
            // Calculating distance and speed
            GoalDistance = Vector3.Distance(GameObject.Find("Goal").transform.position, gameObject.transform.position);
            AgentValues av = new AgentValues()
            {
                dGoal = GoalDistance,
                vGoalX = goalSpeedRelativePointVelocity.x,
                vGoalY = goalSpeedRelativePointVelocity.y,
                vGoalZ = goalSpeedRelativePointVelocity.z,
                ID_Alias = _agentId,
                timestamp = DateTime.Now
            };
            //Debug.Log(appendValuesToContext(av));
            var layerMask = LayerMask.GetMask("Ostacoli", "Omini", "Muri");
            RaycastHit hit;
            // Does the ray intersect any objects excluding the player layer
            var angle = 0f;
            for (int i = 0; i < Rays; i++)
            {
                angle += 2 * Mathf.PI / Rays;
                float xInput = Mathf.Sin(angle);
                float zInput = Mathf.Cos(angle);

                var transformPosition = transform.position;
                transformPosition.y += yShift;
                var directionalVector = new Vector3(xInput, 0, zInput);
                if (Physics.Raycast(transformPosition, transform.TransformDirection(directionalVector), out hit,
                    MaxDistance, layerMask))
                {
                    Enum.TryParse(hit.transform.gameObject.tag, out hitType hittype);
                    if (hittype == hitType.Wall && i == Rays - 1)
                    {
                        WallHit(gameObject);
                    }

                    Debug.DrawRay(transform.position, transform.TransformDirection(directionalVector) * hit.distance,
                        i == Rays - 1 ? Color.blue : Color.red);
                    //Debug.Log("Did Hit");
                }
                else
                {
                    Debug.DrawRay(transformPosition, transform.TransformDirection(directionalVector) * MaxDistance,
                        i == Rays - 1 ? Color.black : Color.green);
                    //Debug.Log("Did not Hit");
                }
            }
        }

        public void WallHit(GameObject hit, float angle = 2 * Mathf.PI / 360)
        {
            angle *= _angle;
            switch (hit.transform.rotation.y)
            {
                case 1:
                    hit.transform.rotation = new Quaternion(hit.transform.rotation.x,
                        -1,
                        hit.transform.rotation.z,
                        hit.transform.rotation.w);
                    break;

                case -1:
                    hit.transform.rotation = new Quaternion(hit.transform.rotation.x,
                        1,
                        hit.transform.rotation.z,
                        hit.transform.rotation.w);
                    break;
            }

            hit.transform.rotation = new Quaternion(hit.transform.rotation.x,
                    hit.transform.rotation.y + Mathf.Sin(angle),
                    hit.transform.rotation.z,
                    hit.transform.rotation.w);
        }

        /// <summary>
        /// TO BE moved in helper project
        /// </summary>
        /// <param name="minimum">min</param>
        /// <param name="maximum">max</param>
        /// <returns></returns>
        public double GetRandomNumber(double minimum, double maximum)
        {
            Random random = new Random();
            return random.NextDouble() * (maximum - minimum) + minimum;
        }
    }
}