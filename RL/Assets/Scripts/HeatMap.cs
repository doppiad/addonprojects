﻿// RL Project
// Ditolve Davide 806953
// Universita' degli studi Milano Bicocca

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts
{
    /// <summary>
    /// Add this class to any surface to draw grid on it
    /// </summary>
    public class HeatMap : MonoBehaviour
    {
        private Vector3 _zeroPosition = new Vector3(-2403, 22, -2519);
        private Vector3 _lastPosition = new Vector3(2597,22,2481);
        private int _rows = 100;
        private int _columns = 100;
        private int _tileSize = 50;
        public Vector3 ZeroPosition { get => _zeroPosition; set => _zeroPosition = value; }
        public Vector3 LastPosition { get => _lastPosition; set => _lastPosition = value; }
        public int Rows { get => _rows; set => _rows = value; }
        public int Columns { get => _columns; set => _columns = value; }
        public int TileSize { get => _tileSize; set => _tileSize = value; }


        /// <summary>
        /// Draw grid on surface with gizmos
        /// </summary>
        private void OnDrawGizmos()
        {
            for (int i = 0; i < Rows; i++)
            {
                Gizmos.DrawLine(new Vector3(_zeroPosition.x + i * TileSize, _zeroPosition.y, _zeroPosition.z), new Vector3(_zeroPosition.x + i * TileSize, _lastPosition.y, _lastPosition.z));
            }
            for (int i = 0; i < Columns; i++)
            {
                Gizmos.DrawLine(new Vector3(_zeroPosition.x, _zeroPosition.y, _zeroPosition.z+i * TileSize), new Vector3(_lastPosition.x, _lastPosition.y, _zeroPosition.z + i * TileSize));
            }
        }
    }
}