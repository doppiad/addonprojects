﻿// RL Project
// Ditolve Davide 806953
// Universita' degli studi Milano Bicocca

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PyConnector;

namespace Assets.Scripts
{

    /// <summary>
    /// This class has been designed only for 100m scene
    /// This is a test class, used to fine tune the walking speed and test Py connector
    /// </summary>
    [Obsolete("This class shouldn't be used.", false)]
    public class WalkingTest : MonoBehaviour
    {
        // Start is called before the first frame update
        private DateTime _time;

        private void Start()
        {
            _time = DateTime.Now;
            Debug.Log(ConnectorPy.testPy());
            var e = Common.CommonFeature.GetLogger();
            e.Error("Prova");
        }

        // Update is called once per frame
        private void Update()
        {
            gameObject.GetComponent<Rigidbody>().velocity = Vector3.back * 55; // 55 ~1.37m/s 1 ~ 0.024909m/s
            gameObject.transform.rotation =
                new Quaternion(transform.rotation.x, 180, transform.rotation.z, transform.rotation.w);
        }

        private void OnTriggerEnter(Collider other)
        {
            Debug.Log(other.name + " after: " + (DateTime.Now - _time).TotalSeconds + "ms");
        }
    }
}