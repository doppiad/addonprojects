﻿// RL Project
// Ditolve Davide 806953
// Universita' degli studi Milano Bicocca

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using RLDAL;
using RLDataLayer;
using UnityEngine;
using UnityEngine.UI;
using Object = System.Object;

namespace Assets.Scripts
{
    public class MainMenuController : MonoBehaviour
    {
        private string _selected;
        private Vector3 _textPosition;
        private Vector3 _inputPosition;
        private GameObject _inputToClone;
        private List<Dropdown.OptionData> behaviors;

        // Start is called before the first frame update
        private void Start()
        {
            _inputToClone = GameObject.Find("InputToClone");
            _textPosition = GameObject.Find("TextToClone").GetComponent<RectTransform>().anchoredPosition3D;
            _inputPosition = _inputToClone.GetComponent<RectTransform>().anchoredPosition3D;
            GameObject.Find("TextToClone").SetActive(false);
            GameObject.Find("InputToClone").SetActive(false);
            //GameObject.Destroy(GameObject.Find("TextToClone"));
            //GameObject.Destroy(GameObject.Find("InputToClone"));
            //GameObject.Find("EditPanel").SetActive(false);
            try
            {
                var behaviorsNames = ConfigManager.getConfigurationNames();
                behaviors = new List<Dropdown.OptionData>();
                if (behaviorsNames != null)
                    foreach (var b in behaviorsNames)
                        behaviors.Add(new Dropdown.OptionData(b));
                GameObject.Find("Behaviors").GetComponent<Dropdown>().options = behaviors;
                _selected = behaviors.First().text;
            }
            catch (Exception e)
            {
                GameObject.Find("Exception").GetComponent<Text>().text = e.Message;
            }
        }

        public void onStartSimulationClick()
        {
        }

        public void onEditConfigClick()
        {
            GameObject.Find("EditPanel").SetActive(true);
            try
            {
                foreach (var toDestroy in GameObject.FindGameObjectsWithTag("MenuElement"))
                    Destroy(toDestroy);
            }
            catch (Exception e)
            {
            }

            Config cf = ConfigManager.GetConfigurationByName(_selected);
            var fields = cf.GetType().GetProperties().Select(x => x.Name);
            int offSet = 0;
            if (fields.Count() > 10)
            {
                GameObject.Find("Content").GetComponent<RectTransform>().sizeDelta = new Vector2(GameObject.Find("Content").GetComponent<RectTransform>().sizeDelta.x, GameObject.Find("Content").GetComponent<RectTransform>().sizeDelta.y + 30 * fields.Count());
            }
            foreach (var c in fields)
            {
                //TEXT FIELD
                GameObject newGO = new GameObject(c);
                newGO.tag = "MenuElement";
                newGO.transform.SetParent(GameObject.Find("Content").transform);
                newGO.AddComponent(typeof(RectTransform));
                newGO.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(_textPosition.x, _textPosition.y - offSet * 30, 0);
                newGO.GetComponent<RectTransform>().sizeDelta = new Vector2(newGO.GetComponent<RectTransform>().sizeDelta.x + 50, newGO.GetComponent<RectTransform>().sizeDelta.y + 5);
                Text t = newGO.AddComponent<Text>();
                t.font = Font.CreateDynamicFontFromOSFont("Arial", 16);
                t.text = c;
                t.fontSize = 16;
                t.alignment = TextAnchor.MiddleCenter;
                //INPUT FIELD
                GameObject newInput = Instantiate(_inputToClone);
                newInput.name = "I_" + c;
                newInput.SetActive(true);
                newInput.transform.SetParent(GameObject.Find("Content").transform);
                newInput.tag = "InputElement";
                newInput.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(_inputPosition.x, _inputPosition.y - offSet * 30, 0);
                newInput.GetComponent<RectTransform>().sizeDelta = new Vector2(newInput.GetComponent<RectTransform>().sizeDelta.x + 50, newInput.GetComponent<RectTransform>().sizeDelta.y + 5);
                newInput.GetComponent<InputField>().text = cf.GetType().GetProperty(c).GetValue(cf, null).ToString();
                newInput.GetComponent<InputField>().onEndEdit.AddListener(delegate { onTextEdit(newInput); });
                //OFFSET
                offSet += 1;
            }
        }

        public void onBehaviorChange(int id)
        {
            _selected = behaviors[id].text;
            Debug.Log(_selected);
            //GameObject.Find("Behaviors").GetComponent<Dropdown>();
        }

        public void onTextEdit(GameObject input)
        {
            ConfigManager.setSingleValue(_selected, input.name.Substring(2), input.GetComponent<InputField>().text);
        }

        // Update is called once per frame
        private void Update()
        {
            try
            {
                var behaviorsNames = ConfigManager.getConfigurationNames();
                List<Dropdown.OptionData> behaviors = new List<Dropdown.OptionData>();
                if (behaviorsNames != null)
                    foreach (var b in behaviorsNames)
                        behaviors.Add(new Dropdown.OptionData(b));
                GameObject.Find("Behaviors").GetComponent<Dropdown>().options = behaviors;
            }
            catch (Exception e)
            {
                GameObject.Find("Exception").GetComponent<Text>().text = e.Message;                
            }
        }
    }
}