﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{

    /// <summary>
    /// This class allow the user to move the camera like unity editor's camera
    /// </summary>
    public class CameraEditor : MonoBehaviour
    {
        [SerializeField]
        private float lookSpeedH = 2f;

        [SerializeField]
        private float lookSpeedV = 2f;

        [SerializeField]
        private float zoomSpeed = 2f;

        [SerializeField]
        private float dragSpeed = 3f;

        private float yaw = 0f;
        private float pitch = 0f;

        private void Start()
        {
            // Initialize the correct initial rotation
            yaw = transform.eulerAngles.y;
            pitch = transform.eulerAngles.x;
        }

        private void Update()
        {
            // Only work with the Left Alt pressed
            if (Input.GetKey(KeyCode.LeftAlt))
            {
                //Look around with Left Mouse
                if (Input.GetMouseButton(0))
                {
                    yaw += lookSpeedH * Input.GetAxis("Mouse X");
                    pitch -= lookSpeedV * Input.GetAxis("Mouse Y");

                    transform.eulerAngles = new Vector3(pitch, yaw, 0f);
                }

                //drag camera around with Middle Mouse
                if (Input.GetMouseButton(2))
                {
                    transform.Translate(-Input.GetAxisRaw("Mouse X") * Time.deltaTime * dragSpeed, -Input.GetAxisRaw("Mouse Y") * Time.deltaTime * dragSpeed, 0);
                }

                if (Input.GetMouseButton(1))
                {
                    //Zoom in and out with Right Mouse
                    transform.Translate(0, 0, Input.GetAxisRaw("Mouse X") * zoomSpeed * .07f, Space.Self);
                }

                //Zoom in and out with Mouse Wheel
                transform.Translate(0, 0, Input.GetAxis("Mouse ScrollWheel") * zoomSpeed, Space.Self);
            }
        }
    }
}