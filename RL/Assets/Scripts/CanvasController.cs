﻿// RL Project
// Ditolve Davide 806953
// Universita' degli studi Milano Bicocca

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
    /// <summary>
    /// This class has been designed to be used in first try without QLearning
    /// </summary>
    [Obsolete("This class is no longer used",false)]
    public class CanvasController : MonoBehaviour
    {
        private GameObject _lastSelected = null;

        public GameObject RemoveButton;

        // Start is called before the first frame update
        private void Start()
        {
        }

        // Update is called once per frame
        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            { // if left button pressed...
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider.name.Contains("."))
                    {
                        try
                        {
                            GameObject.Find("SelectedText").GetComponent<Text>().text = "Selected:\t" + hit.collider.name;
                        }
                        catch (Exception e)
                        {
                            GameObject.Find("SelectedText").GetComponent<Text>().text = "None";
                        }

                        if (_lastSelected == null)
                        {
                            RemoveButton.SetActive(true);
                            _lastSelected = hit.collider.gameObject;
                            Behaviour h = (Behaviour)hit.collider.gameObject.GetComponent("Halo");
                            h.enabled = true;
                            GameObject.Find("RemovePersonButton").GetComponentInChildren<Text>().text =
                                "Remove:\t" + hit.collider.name;
                        }
                        else
                        {
                            Behaviour h = (Behaviour)hit.collider.gameObject.GetComponent("Halo");
                            h.enabled = true;
                            Behaviour h2 = (Behaviour)_lastSelected.GetComponent("Halo");
                            h2.enabled = false;
                            GameObject.Find("RemovePersonButton").GetComponentInChildren<Text>().text =
                                "Remove:\t" + hit.collider.name;
                            _lastSelected = hit.collider.gameObject;
                        }

                        Debug.Log(hit.rigidbody.name);
                        // the object identified by hit.transform was clicked
                        // do whatever you want
                    }
                }
            }
        }
        /// <summary>
        /// Method linked to remove person button click on scene
        /// </summary>
        public void onRemoveClick()
        {
            Destroy(_lastSelected);
            _lastSelected = null;
            RemoveButton.SetActive(false);
        }
        /// <summary>
        /// Method linked to add person button click on scene
        /// </summary>
        public void onAddClick()
        {
            var toClone = GameObject.Find("ToClone");
            System.Random r = new System.Random();
            var randomX = r.Next(400, 700);
            var randomZ = r.Next(350, 620);
            var Cloned = Instantiate(toClone,
                                                    new Vector3(randomX, 0.570f, randomZ),
                                                    Quaternion.Euler(
                                                        0,
                                                        Random.Range(0.0f, 360.0f),
                                                        0));
            Cloned.GetComponent<RayCastClient>().enabled = true;
            Cloned.name = Path.GetRandomFileName();
            Cloned.GetComponent<TrailRenderer>().material.color = new Color((float)r.NextDouble(),
                                                                            (float)r.NextDouble(),
                                                                            (float)r.NextDouble());
            Cloned.SetActive(true);
        }
    }
}