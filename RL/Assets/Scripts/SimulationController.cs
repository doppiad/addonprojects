﻿// RL Project
// Ditolve Davide 806953
// Universita' degli studi Milano Bicocca
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Assets.Scripts;
using Common;
using OfficeOpenXml;
using RLDAL;
using RLDataLayer;
using UnityEngine;

public class SimulationController : MonoBehaviour
{
    private int _sessionId;
    //ADD HERE ALL BUSINESS LOGIC BEFORE ANY OTHER SCRIPT
    private void Awake()
    {
        ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
        _sessionId = SimulationManager.CreateNewSimulation(new Session() { Name = "Editor",IDConfig = 2 });
        AgentValuesManager.setSessionId(_sessionId);

        if (File.Exists(@"C:\Addon Projects\TestFile\qTable.xlsx"))
        {
            var epoch = QLearning.readExistingTable();
            AgentController.setEpoch(epoch);
        }
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Time.timeScale -= 0.1f;
        }
        else if (Input.GetKeyDown(KeyCode.W))
        {
            Time.timeScale -= 1f;
        }
        else if(Input.GetKeyDown(KeyCode.E))
        {
            Time.timeScale += 0.1f;
        }
        else if(Input.GetKeyDown(KeyCode.R))
        {
            Time.timeScale += 1f;
        }
    }

    public int getSessionId()
    {
        return _sessionId;
    }
}