﻿// RL Project
// Ditolve Davide 806953
// Universita' degli studi Milano Bicocca
using RLDataLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using OfficeOpenXml;
using log4net.Repository.Hierarchy;

namespace Common
{
    public static class QLearning
    {
        private static int _states;// = (int)Math.Ceiling((double)((double)25000000 / (double)2500));//25000000;
        private static readonly int _actions = 9;
        private static double[,] _qTable;// = new double[States, Actions];

        private static double _lastDistanceFromGoal;
        private static double _distanceFromGoal;
        private static readonly ILog Log = CommonFeature.GetLogger();

        // Hyperparameters
        private static double _alpha = 0.3;

        private static double _gamma = 0.6;

        private static double _epsilon = 0.125;

        private static double _penalties = 0;
        private static double _reward = 0;
        private static int _actualState = 0;
        private static int _lastState = 0;
        private static int _lastAction = 0;
        private static readonly Random R = new Random();
        private static int _lastEpoch = 0;
        private static double _cumulativeReward = 0;
        private static readonly ExcelPackage Excel = new ExcelPackage(new FileInfo("C:\\Addon Projects\\TestFile\\qTable.xlsx"));

        private static double _oldValue;
        private static bool _checkpoint;
        private static int _goalEpisode;

        public static double ValueToSave;

        private static double minSpeed, maxSpeed;
        public static double MinSpeed { get => minSpeed; set => minSpeed = value; }
        public static double MaxSpeed { get => maxSpeed; set => maxSpeed = value; }

        public static int States { get => _states; set => _states = value; }


        private static int _tilePerRow;
        private static bool _outOfPath;
        private static bool _goalReached;
        public static int Actions => _actions;

        public static double[,] QTable { get => _qTable; set => _qTable = value; }
        public static double Epsilon { get => _epsilon; set => _epsilon = value; }
        public static bool Checkpoint { get => _checkpoint; set => _checkpoint = value; }
        public static double Gamma { get => _gamma; set => _gamma = value; }
        public static double Alpha { get => _alpha; set => _alpha = value; }
        public static bool obstacleHit { get; set; }
        public static int TilePerRow { get => _tilePerRow; set => _tilePerRow = value; }
        public static bool OutOfPath { get => _outOfPath; set => _outOfPath = value; }
        public static bool GoalReached { get => _goalReached; set => _goalReached = value; }
        public static int GoalEpisode { get => _goalEpisode; set => _goalEpisode = value; }

        private static FixedSizedQueue<int> actionsBuffer = new FixedSizedQueue<int>(3);
        private static FixedSizedQueue<double> rewardsBuffer = new FixedSizedQueue<double>(3);
        public static void SetLastEpoch(int epoch)
        {
            Epsilon = 3f / (float)epoch;
            _lastDistanceFromGoal = 0;
            _penalties = 0;
            _lastEpoch = epoch;
            _oldValue = 0;
            _distanceFromGoal = 0;
            _cumulativeReward = 0;
            _outOfPath = false;
            GoalReached = false;
        }

        public static double GetEpsilon()
        {
            return Epsilon;
        }
        public static double GetCumulativeReward()
        {
            return _cumulativeReward;
        }
        public static double GetLastReward()
        {
            return _reward;
        }
        public static void resetReward()
        {
            _reward = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="action">
        /// SpeedUp 0  -
        /// SpeedUnchanged 1 -
        /// SpeedDown 2 -
        /// SpeedZero 3
        /// <para>&#160;</para> 
        /// RotationLeftStrong 4 -
        /// RotationLeft 5 - 
        /// RotationZero 6 -
        /// RotationRightStrong 7 -
        /// RotationRight 8</param>
        /// <param name="speed"></param>
        /// <param name="wallHit"></param>
        public static void GetReward(double speed, bool wallHit)
        {
            int action = getLastAction();
            double deltaDistance = _lastDistanceFromGoal - _distanceFromGoal;
            //Log.Info("Delta distance: " + deltaDistance);
            if (deltaDistance >= 0)
                _reward = (deltaDistance) * 1000;
            else
                _reward = (deltaDistance) * 10;
            if (_distanceFromGoal <= 72)
            {
                _reward += 10000;
            }
            if (GoalReached)
            {
                _reward += 1000 * GoalEpisode;
            }

            if (action < 4)
                speedReward(action, speed);
            else
                rotationReward(action, deltaDistance);
            if (anomalyCheck())
            {
                _reward -= 200;
            }
            if (wallHit || obstacleHit)
            {
                //_penalties += Math.Abs(_reward / 2);
                _reward -= 50;
            }
            if (Checkpoint)
            {
                _penalties /= 2;
                _reward += Math.Abs(_reward / 4);
            }
            if (OutOfPath)
            {
                _reward -= 100;
            }
            //Log.Error("Report variabili: speed: " + speed + " action: " + action + " penalties: " + _penalties + " distanza goal: " + _distanceFromGoal + " ultima distanza: " + _lastDistanceFromGoal + " Reward: " + _reward);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="action">
        /// SpeedUp 0  -
        /// SpeedUnchanged 1 -
        /// SpeedDown 2 -
        /// SpeedZero 3
        /// </param>
        public static void speedReward(int action, double speed)
        {
            if ((action == 0 || action == 2) && speed<=maxSpeed && speed >=minSpeed && _reward > 0)
            {
                _reward = _reward / 1.3f;
            }
            if (speed > maxSpeed) //too fast
            {
                _penalties += 0.5;
                _reward -= (speed - maxSpeed) * (Math.Log10((3 * (speed - maxSpeed)) + 2) * 10);
            }
            else if (speed < minSpeed) //too slow
            {
                _penalties += 0.5;
                _reward -= (minSpeed - speed) * (Math.Log10((3 * (minSpeed - speed)) + 2) * 10);
            }
            else //speed ok
                _reward += 100;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="action">
        /// RotationLeftStrong 4 -
        /// RotationLeft 5 - 
        /// RotationZero 6 -
        /// RotationRightStrong 7 -
        /// RotationRight 8
        /// </param>
        /// <param name="deltaDistance"></param>
        public static void rotationReward(int action, double deltaDistance)
        {
            if (deltaDistance < 0 && (action == 4 || action == 5 || action == 7 || action == 8))
            {
                if (_reward > 0)
                    _reward = _reward / 1.12f;
                else
                    _reward = _reward * 1.12f;
            }
            else if(deltaDistance >= 0 && (action == 4 || action == 5 || action == 7 || action == 8))
            {
                if (_reward > 0)
                    _reward = _reward * 1.15f;
                else
                    _reward = _reward / 1.15f;
            }
        }
        public static int readExistingTable(string path = @"C:\Addon Projects\TestFile\qTable.xlsx")
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage e = new ExcelPackage(new FileInfo(path));
            var ws = e.Workbook.Worksheets.First();
            var lastEpoch = int.Parse(ws.Name) + 1;
            var start = DateTime.Now;
            for (int i = 0; i < States; i++)
            {
                for (int k = 0; k < 9; k++)
                {
                    QTable[i, k] = ws.Cells[i + 1, k + 1].Value != null ? (double)ws.Cells[i + 1, k + 1].Value : 0;
                }
            }
            var finish = DateTime.Now;
            _lastEpoch = lastEpoch;
            counter = lastEpoch;
            return lastEpoch;

        }
        public static int GetAction(AgentValues ag)
        {

            int action = -1;
            List<int> zeros = new List<int>();
            double max = double.MinValue;
            _lastDistanceFromGoal = ag.dGoal;
            try
            {
                if (R.NextDouble() < Epsilon) //new actions
                {
                    action = R.Next(0, 8);
                }
                else //already known actions
                {
                    _actualState = GetCurrentState(ag.xPosition, ag.zPosition);
                    for (int i = 0; i < 9; i++)
                    {
                        var tmp = QTable[_actualState, i];

                        if (tmp > max)
                        {
                            max = QTable[_actualState, i];
                            action = i;
                        }
                        else if (tmp == 0.0d)
                        {
                            zeros.Add(i);
                        }
                    }

                    if (zeros.Any() && max == 0.0d)
                    {
                        action = zeros[R.Next(zeros.Count)];
                    }
                }
                actionsBuffer.Enqueue(action);
                _oldValue = QTable[_actualState, action];
                _lastAction = action;
                _lastState = _actualState;

                return action;
            }
            catch (Exception e)
            {
                Log.Error("Action fallito", e);
                return R.Next(0, 8);
            }
        }

        public static int Train(AgentValues ag)
        {

            if (ag.Episode == 0)
                return -1;
            _distanceFromGoal = ag.dGoal;
            try
            {
                double max = double.MinValue;
                _actualState = GetCurrentState(ag.xPosition, ag.zPosition);
                for (int i = 0; i < 9; i++)
                {
                    if (QTable[_actualState, i] > max)
                    {
                        max = QTable[_actualState, i];
                    }
                }
                GetReward(ag.speed, ag.wallHit);
                double newValue = ((1 - Alpha) * _oldValue) + Alpha * (_reward + Gamma * max) - _penalties;
                _cumulativeReward += newValue;
                QTable[_lastState, _lastAction] = newValue;
                rewardsBuffer.Enqueue(newValue);
                ValueToSave = newValue;
                ExportToExcel();

                return _actualState;
            }
            catch (Exception e)
            {
                Log.Error("Train fallito", e);
                return R.Next(0, 8);
            }
        }

        public static int getLastAction()
        {
            return actionsBuffer.Last();
        }
        public static int getFirstAction()
        {
            return actionsBuffer.First();
        }
        public static bool repetitionCheck()
        {
            return actionsBuffer.Count != actionsBuffer.Distinct().Count();
        }

        /// SpeedUp 0  -
        /// SpeedUnchanged 1 -
        /// SpeedDown 2 -
        /// SpeedZero 3
        /// <para>&#160;</para> 
        /// RotationLeftStrong 4 -
        /// RotationLeft 5 - 
        /// RotationZero 6 -
        /// RotationRightStrong 7 -
        /// RotationRight 8
        public static bool anomalyCheck()
        {
            if ((actionsBuffer.Contains(4) || actionsBuffer.Contains(5)) && (actionsBuffer.Contains(7) || actionsBuffer.Contains(8)))
                return true;
            else if ((actionsBuffer.Contains(0) && actionsBuffer.Contains(2)))
                return true;
            else
                return false;
        }
        public static int GetCurrentState(int x, int z)
        {
            return (x) + Math.Abs((z) * TilePerRow);
        }
        private static int counter = 0;


        public static void ExportToExcel()
        {

            int s = _lastState;
            int a = _lastAction;
            double v = ValueToSave;
            ExcelWorksheet workSheet;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            // name of the sheet
            if (counter == _lastEpoch)
            {
                workSheet = _lastEpoch > 0
                    ? Excel.Workbook.Worksheets.Add(_lastEpoch.ToString(), Excel.Workbook.Worksheets.FirstOrDefault())
                    : Excel.Workbook.Worksheets.Add(_lastEpoch.ToString());
                if (_lastEpoch > 0)
                    Excel.Workbook.Worksheets.Delete(Excel.Workbook.Worksheets.FirstOrDefault().Index);
                workSheet.DefaultRowHeight = 12;
                Excel.Save();
                counter++;
            }
            else
            {
                workSheet = Excel.Workbook.Worksheets[Excel.Workbook.Worksheets.FirstOrDefault().Index];
            }

            workSheet.SetValue(s + 1, a + 1, v);
        }
    }
}