﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    /// <summary>
    /// This class is a special queue where when an item is enqueued and there are no empty cell
    /// oldest item will be automatically dequeued.
    /// Thanks to: Dave Lawrence
    /// https://stackoverflow.com/questions/5852863/fixed-size-queue-which-automatically-dequeues-old-values-upon-new-enques
    /// </summary>
    /// <typeparam name="T">This class will work with any Type</typeparam>
    public class FixedSizedQueue<T> : ConcurrentQueue<T>
    {
        /// <summary>
        /// Object to lock before dequeueing
        /// </summary>
        private readonly object syncObject = new object();
        /// <summary>
        /// List Size
        /// </summary>
        public int Size { get; private set; }

        /// <summary>
        /// Size is mandatory so this is the only ctor
        /// </summary>
        /// <param name="size">List size</param>
        public FixedSizedQueue(int size)
        {
            Size = size;
        }
        /// <summary>
        /// This method will enqueue obj and check if list is full
        /// in that case oldest item will be dequeued
        /// </summary>
        /// <param name="obj"></param>
        public new void Enqueue(T obj)
        {
            base.Enqueue(obj);
            lock (syncObject)
            {
                while (base.Count > Size)
                {
                    T outObj;
                    base.TryDequeue(out outObj);
                }
            }
        }
    }
}
