﻿using Newtonsoft.Json;
using RLWebProject.CustomController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RLWebProject.Views
{
    public class ExperimentsController : CustomDBController
    {
        //
        // GET: /Experiments/
        public ActionResult E1()
        {
            ViewBag.session = db.Session.Count(x => x.Name == "Test Object");
            ViewBag.epoch = db.AgentValues.Where(x => x.Agent.Session.Name == "Test Object").Max(x => x.Epoch);
            ViewBag.episode = db.AgentValues.Where(x => x.Agent.Session.Name == "Test Object").Max(x => x.Episode);
            ViewBag.sessions = db.Session.Where(x => x.Name == "Test Object" && x.Agents.Any()).ToList();
            ViewBag.config = db.Config.FirstOrDefault().ToString();
            return View();
        }

        public int loadEpoch(int id)
        {
            var toExamine = db.AgentValues.Where(x => x.Agent.ID_Session == id).ToList();
            return toExamine.Max(x => x.Epoch);
        }
        public string loadPie(int id, int epoch)
        {
            var toCycle = db.AgentValues.Where(x => x.Agent.Session.ID == id && x.Epoch == epoch);

            var query = toCycle.GroupBy(n => n.lastDecision,
            (key, values) => new { key = key, Count = values.Count() }).ToArray();

            return JsonConvert.SerializeObject(query);
        }
        public string loadAllPie(int id)
        {
            var toCycle = db.AgentValues.Where(x => x.Agent.Session.ID == id);

            var query = toCycle.GroupBy(n => n.lastDecision,
            (key, values) => new { key = key, Count = values.Count() }).ToArray();

            return JsonConvert.SerializeObject(query);
        }
        public string loadAllReward(int id)
        {
            var toCycle = db.AgentValues.Where(x => x.Agent.Session.ID == id);
            var maxEpisode = db.AgentValues.Where(x => x.Agent.Session.ID == id).Max(x => x.Episode);
            var query = toCycle.Where(x => x.Episode == maxEpisode).Select(x=>x.cumulativeReward??0);
            int i = 0;
            var toReturn = new
            {
                data = query,
                label = "Epoch " + i,
                borderColor = randomColor(),
                fill = false
            };

            return JsonConvert.SerializeObject(toReturn);
        }
        public string loadReward(int id,int epoch)
        {
            var toCycle = db.AgentValues.Where(x => x.Agent.Session.ID == id && x.Epoch == epoch);
            var query = toCycle.Select(x => x.cumulativeReward ?? 0);
            int i = 0;
            var toReturn = new
            {
                data = query,
                label = "Episode " + i,
                borderColor = randomColor(),
                fill = false
            };

            return JsonConvert.SerializeObject(toReturn);
        }
        public string loadAllSpeed(int id)
        {
            var toCycle = db.AgentValues.Where(x => x.Agent.Session.ID == id);
            int howMany = (int)Math.Ceiling((double)toCycle.Max(x => x.Epoch) / 5);
            var toReturn = new chartData[howMany];
            for (int i = 0; i < toCycle.Max(x => x.Epoch); i += 5)
            {
                var tt = toCycle.Where(x => x.Epoch == i);
                var data = tt.Select(x => Math.Round(x.speed, 3)).ToArray();
                chartData toFill = new chartData
                {
                    data = data,
                    label = "Epoch " + i,
                    borderColor = randomColor(),
                    fill = false
                };
                toReturn[i / 5] = toFill;
            }
            return JsonConvert.SerializeObject(toReturn);
        }
        
        public string loadSpeed(int id, int epoch)
        {
            var toCycle = db.AgentValues.Where(x => x.Agent.Session.ID == id && x.Epoch == epoch);
            var toReturn = new chartData[1];
            var tt = toCycle;
            var data = tt.Select(x => Math.Round(x.speed, 3)).ToArray();
            chartData toFill = new chartData
            {
                data = data,
                label = "Test" + epoch,
                borderColor = randomColor(),
                fill = false
            };
            toReturn[0] = toFill;
            return JsonConvert.SerializeObject(toReturn);
        }
        public string getPieData(int epoch = 1)
        {
            var toCycle = db.AgentValues.Where(x => x.Agent.Session.ID == 2172);

            var query = toCycle.GroupBy(n => n.lastDecision,
            (key, values) => new { key = key, Count = values.Count() }).Select(x => x.Count).ToArray();

            return JsonConvert.SerializeObject(query);
        }
        public string randomColor()
        {
            var random = new Random();
            var color = String.Format("#{0:X6}", random.Next(0x1000000)); // = "#A197B9"
            return color;
        }
    }
    public class chartData
    {
        public double[] data = new double[80];
        public string label;
        public string borderColor;
        public bool fill;
    }
    public class boxPlotData
    {
        public double min;
        public double q1;
        public double median;
        public double q3;
        public double max;
        public double[] outliers;
    }

}