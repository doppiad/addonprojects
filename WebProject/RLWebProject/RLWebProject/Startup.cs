﻿using Common;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RLWebProject.Startup))]
namespace RLWebProject
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            RLDataLayer.RLDatabase.Create().Database.CreateIfNotExists();
            ConfigureAuth(app);
            var log = CommonFeature.GetLogger("Progetto Web");
            log.Error("Test nome");
        }
    }
}
