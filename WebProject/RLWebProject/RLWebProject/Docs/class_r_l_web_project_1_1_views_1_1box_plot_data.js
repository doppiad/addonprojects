var class_r_l_web_project_1_1_views_1_1box_plot_data =
[
    [ "max", "class_r_l_web_project_1_1_views_1_1box_plot_data.html#a3c24d508ad92fe1e5883fe79598517f6", null ],
    [ "median", "class_r_l_web_project_1_1_views_1_1box_plot_data.html#a343cf8a95013877cccbe371650e6143c", null ],
    [ "min", "class_r_l_web_project_1_1_views_1_1box_plot_data.html#ae02f06f1c2cc459b0e9fb1b364e849bb", null ],
    [ "outliers", "class_r_l_web_project_1_1_views_1_1box_plot_data.html#aa9e0288125b93d3738d877493fbf0e0d", null ],
    [ "q1", "class_r_l_web_project_1_1_views_1_1box_plot_data.html#aa8bb1dcf3c50997220c0374924c3c6b0", null ],
    [ "q3", "class_r_l_web_project_1_1_views_1_1box_plot_data.html#a740fa0db3b40742570ca6ebbce3ddf83", null ]
];