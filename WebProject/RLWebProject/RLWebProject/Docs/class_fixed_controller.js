var class_fixed_controller =
[
    [ "Awake", "class_fixed_controller.html#a160b383087bb7c37b77e47c5a7429b7e", null ],
    [ "FixedUpdate", "class_fixed_controller.html#adae6b002ec4f1de300e1ee87fbc30a19", null ],
    [ "Loop", "class_fixed_controller.html#ae7c5f38ddd17a680b05e2f18be8b34d9", null ],
    [ "OnGUI", "class_fixed_controller.html#ad10c2d0b0d3bc9283cd023670389309d", null ],
    [ "Update", "class_fixed_controller.html#a1aacc32f6565ee11e86104cf1fc5913b", null ],
    [ "expectedFPS", "class_fixed_controller.html#a0b9aaf7284101a8d0b3680cfab6145c1", null ],
    [ "fixedUpdateCount", "class_fixed_controller.html#a02a052cd7cd4cb37f6b152a335536b4c", null ],
    [ "updateCount", "class_fixed_controller.html#a4d53a18d5baecb766d699eef97fed8b2", null ],
    [ "updateFixedUpdateCountPerSecond", "class_fixed_controller.html#a80a0dd2d51a46ea2f50748ddd52fd1d2", null ],
    [ "updateUpdateCountPerSecond", "class_fixed_controller.html#a92b09abb86599b85be7cfc7be365da7f", null ]
];