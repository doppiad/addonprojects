var indexSectionsWithContent =
{
  0: "_abcdefghiklmnopqrstuvwxyz",
  1: "abcefhilmqrstvw",
  2: "acpr",
  3: "_abcefghilmpqrstw",
  4: "acdefghilmorstuvw",
  5: "_abcdefgiklmopqrstuvwxyz",
  6: "or",
  7: "dhm",
  8: "aceoprsw",
  9: "_abcdehilmnoprstuvwxz",
  10: "dlprw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "properties",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Properties",
  10: "Pages"
};

