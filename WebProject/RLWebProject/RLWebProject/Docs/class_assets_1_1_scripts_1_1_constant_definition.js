var class_assets_1_1_scripts_1_1_constant_definition =
[
    [ "REACHEDMAXEPOCH", "class_assets_1_1_scripts_1_1_constant_definition.html#abb65f41618e345deb8e3c60ab2df370c", null ],
    [ "ROTATIONLEFT", "class_assets_1_1_scripts_1_1_constant_definition.html#aa76e73d5c2b0b81b9b28c9fb586fafdc", null ],
    [ "ROTATIONLEFTSTRONG", "class_assets_1_1_scripts_1_1_constant_definition.html#a7b5b9398be9d9d132dfec938feaf9c6e", null ],
    [ "ROTATIONRIGHT", "class_assets_1_1_scripts_1_1_constant_definition.html#adf3e2e762f129e29930b0e3adf1ec588", null ],
    [ "ROTATIONRIGHTSTRONG", "class_assets_1_1_scripts_1_1_constant_definition.html#add4f04e5d50b5d054cdff0a0238e7529", null ],
    [ "ROTATIONZERO", "class_assets_1_1_scripts_1_1_constant_definition.html#ae18ae517a2724824a3375b44ae5ce59c", null ],
    [ "SPEEDDOWN", "class_assets_1_1_scripts_1_1_constant_definition.html#aafc29e74fbe22d1a5072a02ecd3f0d14", null ],
    [ "SPEEDUNCHANGED", "class_assets_1_1_scripts_1_1_constant_definition.html#a6f0dd03d451cc8b214c4cd6aad97eb48", null ],
    [ "SPEEDUP", "class_assets_1_1_scripts_1_1_constant_definition.html#a41b773783e433b6b61f1945ae8051f1b", null ],
    [ "SPEEDZERO", "class_assets_1_1_scripts_1_1_constant_definition.html#a643d5d03e8453510bd0ea718d6016105", null ]
];