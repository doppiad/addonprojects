/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "RL FULL PROJECT", "index.html", [
    [ "Developed by:", "md__c_o_n_t_r_i_b_u_t_i_n_g.html", [
      [ "Relator:", "md__c_o_n_t_r_i_b_u_t_i_n_g.html#autotoc_md1", null ],
      [ "Contributors:", "md__c_o_n_t_r_i_b_u_t_i_n_g.html#autotoc_md2", null ]
    ] ],
    [ "WELCOME", "md__r_e_a_d_m_e.html", [
      [ "Folders", "md__r_e_a_d_m_e.html#autotoc_md4", null ],
      [ "Unity Project RL", "md__r_e_a_d_m_e.html#autotoc_md5", null ],
      [ "Web Project", "md__r_e_a_d_m_e.html#autotoc_md6", null ],
      [ "Common", "md__r_e_a_d_m_e.html#autotoc_md7", null ],
      [ "PyConnector", "md__r_e_a_d_m_e.html#autotoc_md8", null ],
      [ "RLDAL", "md__r_e_a_d_m_e.html#autotoc_md9", null ],
      [ "RLDataLayer", "md__r_e_a_d_m_e.html#autotoc_md10", null ],
      [ "General information", "md__r_e_a_d_m_e.html#autotoc_md11", null ]
    ] ],
    [ "RL DATA LAYER", "md__r_l_data_layer_readme.html", null ],
    [ "Polyform Noncommercial License 1.0.0", "md__web_project__r_l_web_project_packages__e_p_plus_85_80_83_license.html", [
      [ "Acceptance", "md__web_project__r_l_web_project_packages__e_p_plus_85_80_83_license.html#autotoc_md14", null ],
      [ "Copyright License", "md__web_project__r_l_web_project_packages__e_p_plus_85_80_83_license.html#autotoc_md15", null ],
      [ "Distribution License", "md__web_project__r_l_web_project_packages__e_p_plus_85_80_83_license.html#autotoc_md16", null ],
      [ "Notices", "md__web_project__r_l_web_project_packages__e_p_plus_85_80_83_license.html#autotoc_md17", null ],
      [ "Changes and New Works License", "md__web_project__r_l_web_project_packages__e_p_plus_85_80_83_license.html#autotoc_md18", null ],
      [ "Patent License", "md__web_project__r_l_web_project_packages__e_p_plus_85_80_83_license.html#autotoc_md19", null ],
      [ "Noncommercial Purposes", "md__web_project__r_l_web_project_packages__e_p_plus_85_80_83_license.html#autotoc_md20", null ],
      [ "Personal Uses", "md__web_project__r_l_web_project_packages__e_p_plus_85_80_83_license.html#autotoc_md21", null ],
      [ "Noncommercial Organizations", "md__web_project__r_l_web_project_packages__e_p_plus_85_80_83_license.html#autotoc_md22", null ],
      [ "Fair Use", "md__web_project__r_l_web_project_packages__e_p_plus_85_80_83_license.html#autotoc_md23", null ],
      [ "No Other Rights", "md__web_project__r_l_web_project_packages__e_p_plus_85_80_83_license.html#autotoc_md24", null ],
      [ "Patent Defense", "md__web_project__r_l_web_project_packages__e_p_plus_85_80_83_license.html#autotoc_md25", null ],
      [ "Violations", "md__web_project__r_l_web_project_packages__e_p_plus_85_80_83_license.html#autotoc_md26", null ],
      [ "No Liability", "md__web_project__r_l_web_project_packages__e_p_plus_85_80_83_license.html#autotoc_md27", null ],
      [ "Definitions", "md__web_project__r_l_web_project_packages__e_p_plus_85_80_83_license.html#autotoc_md28", null ]
    ] ],
    [ "LICENSE", "md__web_project__r_l_web_project_packages__newtonsoft_8_json_812_80_82__l_i_c_e_n_s_e.html", null ],
    [ "LICENSE", "md__web_project__r_l_web_project_packages__newtonsoft_8_json_812_80_83__l_i_c_e_n_s_e.html", null ],
    [ "README", "md__web_project__r_l_web_project_packages_popper_8js_81_816_81_content__scripts__r_e_a_d_m_e.html", null ],
    [ "README", "md__web_project__r_l_web_project__r_l_web_project_obj__release__package__package_tmp__scripts__r_e_a_d_m_e.html", null ],
    [ "README", "md__web_project__r_l_web_project__r_l_web_project__scripts__r_e_a_d_m_e.html", null ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ],
      [ "Package Functions", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Properties", "functions_prop.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Functions", "globals_func.html", null ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_account_controller_8cs.html",
"class_common_1_1_q_learning.html#a97870122cef5e70ef0a85f74ee846820",
"class_r_l_web_project_1_1_models_1_1_register_view_model.html#a279151f1d874d0c04a75624678651c0c",
"namespace_py_connector.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';