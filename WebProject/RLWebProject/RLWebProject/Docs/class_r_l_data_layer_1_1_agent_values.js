var class_r_l_data_layer_1_1_agent_values =
[
    [ "AgentValues", "class_r_l_data_layer_1_1_agent_values.html#a5605933a84fb061d409d627e2fff9e4e", null ],
    [ "AgentValues", "class_r_l_data_layer_1_1_agent_values.html#ac1f439d44bb3e641a6758b8659c691fe", null ],
    [ "ToString", "class_r_l_data_layer_1_1_agent_values.html#a813d8c0f3e243e4c899792105979153d", null ],
    [ "VariablesList", "class_r_l_data_layer_1_1_agent_values.html#a2f2e71ad14a02094990abcd65e7c745b", null ],
    [ "Agent", "class_r_l_data_layer_1_1_agent_values.html#ab861daddb2c3b4b6e4ef36a5fc67f10c", null ],
    [ "cumulativeReward", "class_r_l_data_layer_1_1_agent_values.html#abb815d7d6d06d6c3ae534bc54a4453e5", null ],
    [ "dGoal", "class_r_l_data_layer_1_1_agent_values.html#a5d4c283c50257e40d544c57a03737eb0", null ],
    [ "Episode", "class_r_l_data_layer_1_1_agent_values.html#aa26679c1582dbc522276a763fdf36426", null ],
    [ "Epoch", "class_r_l_data_layer_1_1_agent_values.html#a4e8304fe4b7e9cb8f4490b19c79f3c85", null ],
    [ "ID", "class_r_l_data_layer_1_1_agent_values.html#af02573ef27f810bd7e8699e15901c9d5", null ],
    [ "ID_Alias", "class_r_l_data_layer_1_1_agent_values.html#ad6f45db317c5be4eb496d4b0e9ee4456", null ],
    [ "lastDecision", "class_r_l_data_layer_1_1_agent_values.html#a6b48d046054dfcc0a3492ddbaeff0f8d", null ],
    [ "simulationSpeed", "class_r_l_data_layer_1_1_agent_values.html#a7220f55b971268f1fae803ac35985b02", null ],
    [ "speed", "class_r_l_data_layer_1_1_agent_values.html#a72088bfc2b78249783d9b2562dd8fca0", null ],
    [ "timestamp", "class_r_l_data_layer_1_1_agent_values.html#a259b8f755c6e9bad74674d3946903a04", null ],
    [ "vGoalX", "class_r_l_data_layer_1_1_agent_values.html#a870a3c002485d1f41299672c3d83f686", null ],
    [ "vGoalY", "class_r_l_data_layer_1_1_agent_values.html#aeb5377b5a54d827ea3bafc210fe368bb", null ],
    [ "vGoalZ", "class_r_l_data_layer_1_1_agent_values.html#aabb586a80b94b03501f9793dcebfff5b", null ],
    [ "xPosition", "class_r_l_data_layer_1_1_agent_values.html#a2351d6a1da0473fe14ab3b7d6cdb9b92", null ],
    [ "zPosition", "class_r_l_data_layer_1_1_agent_values.html#ad1d0da1ca72f9f133911b0c6a2958b24", null ]
];