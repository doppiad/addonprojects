var namespaces_dup =
[
    [ "Wut? Poppers?", "md__web_project__r_l_web_project_packages_popper_8js_81_816_81_content__scripts__r_e_a_d_m_e.html#autotoc_md29", null ],
    [ "So, yet another tooltip library?", "md__web_project__r_l_web_project_packages_popper_8js_81_816_81_content__scripts__r_e_a_d_m_e.html#autotoc_md30", [
      [ "Popper.js", "md__web_project__r_l_web_project_packages_popper_8js_81_816_81_content__scripts__r_e_a_d_m_e.html#autotoc_md31", null ],
      [ "Tooltip.js", "md__web_project__r_l_web_project_packages_popper_8js_81_816_81_content__scripts__r_e_a_d_m_e.html#autotoc_md32", null ]
    ] ],
    [ "Installation", "md__web_project__r_l_web_project_packages_popper_8js_81_816_81_content__scripts__r_e_a_d_m_e.html#autotoc_md33", [
      [ "Dist targets", "md__web_project__r_l_web_project_packages_popper_8js_81_816_81_content__scripts__r_e_a_d_m_e.html#autotoc_md34", null ]
    ] ],
    [ "Usage", "md__web_project__r_l_web_project_packages_popper_8js_81_816_81_content__scripts__r_e_a_d_m_e.html#autotoc_md35", [
      [ "Callbacks", "md__web_project__r_l_web_project_packages_popper_8js_81_816_81_content__scripts__r_e_a_d_m_e.html#autotoc_md36", null ],
      [ "Writing your own modifiers", "md__web_project__r_l_web_project_packages_popper_8js_81_816_81_content__scripts__r_e_a_d_m_e.html#autotoc_md37", null ],
      [ "React, Vue.js, Angular, AngularJS, Ember.js (etc...) integration", "md__web_project__r_l_web_project_packages_popper_8js_81_816_81_content__scripts__r_e_a_d_m_e.html#autotoc_md38", null ],
      [ "Migration from Popper.js v0", "md__web_project__r_l_web_project_packages_popper_8js_81_816_81_content__scripts__r_e_a_d_m_e.html#autotoc_md39", null ],
      [ "Performances", "md__web_project__r_l_web_project_packages_popper_8js_81_816_81_content__scripts__r_e_a_d_m_e.html#autotoc_md40", null ]
    ] ],
    [ "Notes", "md__web_project__r_l_web_project_packages_popper_8js_81_816_81_content__scripts__r_e_a_d_m_e.html#autotoc_md41", [
      [ "Libraries using Popper.js", "md__web_project__r_l_web_project_packages_popper_8js_81_816_81_content__scripts__r_e_a_d_m_e.html#autotoc_md42", null ],
      [ "Credits", "md__web_project__r_l_web_project_packages_popper_8js_81_816_81_content__scripts__r_e_a_d_m_e.html#autotoc_md43", null ],
      [ "Copyright and license", "md__web_project__r_l_web_project_packages_popper_8js_81_816_81_content__scripts__r_e_a_d_m_e.html#autotoc_md44", null ]
    ] ],
    [ "Wut? Poppers?", "md__web_project__r_l_web_project__r_l_web_project_obj__release__package__package_tmp__scripts__r_e_a_d_m_e.html#autotoc_md45", null ],
    [ "So, yet another tooltip library?", "md__web_project__r_l_web_project__r_l_web_project_obj__release__package__package_tmp__scripts__r_e_a_d_m_e.html#autotoc_md46", [
      [ "Popper.js", "md__web_project__r_l_web_project__r_l_web_project_obj__release__package__package_tmp__scripts__r_e_a_d_m_e.html#autotoc_md47", null ],
      [ "Tooltip.js", "md__web_project__r_l_web_project__r_l_web_project_obj__release__package__package_tmp__scripts__r_e_a_d_m_e.html#autotoc_md48", null ]
    ] ],
    [ "Installation", "md__web_project__r_l_web_project__r_l_web_project_obj__release__package__package_tmp__scripts__r_e_a_d_m_e.html#autotoc_md49", [
      [ "Dist targets", "md__web_project__r_l_web_project__r_l_web_project_obj__release__package__package_tmp__scripts__r_e_a_d_m_e.html#autotoc_md50", null ]
    ] ],
    [ "Usage", "md__web_project__r_l_web_project__r_l_web_project_obj__release__package__package_tmp__scripts__r_e_a_d_m_e.html#autotoc_md51", [
      [ "Callbacks", "md__web_project__r_l_web_project__r_l_web_project_obj__release__package__package_tmp__scripts__r_e_a_d_m_e.html#autotoc_md52", null ],
      [ "Writing your own modifiers", "md__web_project__r_l_web_project__r_l_web_project_obj__release__package__package_tmp__scripts__r_e_a_d_m_e.html#autotoc_md53", null ],
      [ "React, Vue.js, Angular, AngularJS, Ember.js (etc...) integration", "md__web_project__r_l_web_project__r_l_web_project_obj__release__package__package_tmp__scripts__r_e_a_d_m_e.html#autotoc_md54", null ],
      [ "Migration from Popper.js v0", "md__web_project__r_l_web_project__r_l_web_project_obj__release__package__package_tmp__scripts__r_e_a_d_m_e.html#autotoc_md55", null ],
      [ "Performances", "md__web_project__r_l_web_project__r_l_web_project_obj__release__package__package_tmp__scripts__r_e_a_d_m_e.html#autotoc_md56", null ]
    ] ],
    [ "Notes", "md__web_project__r_l_web_project__r_l_web_project_obj__release__package__package_tmp__scripts__r_e_a_d_m_e.html#autotoc_md57", [
      [ "Libraries using Popper.js", "md__web_project__r_l_web_project__r_l_web_project_obj__release__package__package_tmp__scripts__r_e_a_d_m_e.html#autotoc_md58", null ],
      [ "Credits", "md__web_project__r_l_web_project__r_l_web_project_obj__release__package__package_tmp__scripts__r_e_a_d_m_e.html#autotoc_md59", null ],
      [ "Copyright and license", "md__web_project__r_l_web_project__r_l_web_project_obj__release__package__package_tmp__scripts__r_e_a_d_m_e.html#autotoc_md60", null ]
    ] ],
    [ "Wut? Poppers?", "md__web_project__r_l_web_project__r_l_web_project__scripts__r_e_a_d_m_e.html#autotoc_md61", null ],
    [ "So, yet another tooltip library?", "md__web_project__r_l_web_project__r_l_web_project__scripts__r_e_a_d_m_e.html#autotoc_md62", [
      [ "Popper.js", "md__web_project__r_l_web_project__r_l_web_project__scripts__r_e_a_d_m_e.html#autotoc_md63", null ],
      [ "Tooltip.js", "md__web_project__r_l_web_project__r_l_web_project__scripts__r_e_a_d_m_e.html#autotoc_md64", null ]
    ] ],
    [ "Installation", "md__web_project__r_l_web_project__r_l_web_project__scripts__r_e_a_d_m_e.html#autotoc_md65", [
      [ "Dist targets", "md__web_project__r_l_web_project__r_l_web_project__scripts__r_e_a_d_m_e.html#autotoc_md66", null ]
    ] ],
    [ "Usage", "md__web_project__r_l_web_project__r_l_web_project__scripts__r_e_a_d_m_e.html#autotoc_md67", [
      [ "Callbacks", "md__web_project__r_l_web_project__r_l_web_project__scripts__r_e_a_d_m_e.html#autotoc_md68", null ],
      [ "Writing your own modifiers", "md__web_project__r_l_web_project__r_l_web_project__scripts__r_e_a_d_m_e.html#autotoc_md69", null ],
      [ "React, Vue.js, Angular, AngularJS, Ember.js (etc...) integration", "md__web_project__r_l_web_project__r_l_web_project__scripts__r_e_a_d_m_e.html#autotoc_md70", null ],
      [ "Migration from Popper.js v0", "md__web_project__r_l_web_project__r_l_web_project__scripts__r_e_a_d_m_e.html#autotoc_md71", null ],
      [ "Performances", "md__web_project__r_l_web_project__r_l_web_project__scripts__r_e_a_d_m_e.html#autotoc_md72", null ]
    ] ],
    [ "Notes", "md__web_project__r_l_web_project__r_l_web_project__scripts__r_e_a_d_m_e.html#autotoc_md73", [
      [ "Libraries using Popper.js", "md__web_project__r_l_web_project__r_l_web_project__scripts__r_e_a_d_m_e.html#autotoc_md74", null ],
      [ "Credits", "md__web_project__r_l_web_project__r_l_web_project__scripts__r_e_a_d_m_e.html#autotoc_md75", null ],
      [ "Copyright and license", "md__web_project__r_l_web_project__r_l_web_project__scripts__r_e_a_d_m_e.html#autotoc_md76", null ]
    ] ],
    [ "Assets", "namespace_assets.html", "namespace_assets" ],
    [ "Common", "namespace_common.html", null ],
    [ "PyConnector", "namespace_py_connector.html", null ],
    [ "RLDAL", "namespace_r_l_d_a_l.html", null ],
    [ "RLDataLayer", "namespace_r_l_data_layer.html", null ],
    [ "RLWebProject", "namespace_r_l_web_project.html", "namespace_r_l_web_project" ]
];