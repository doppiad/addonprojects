﻿// RL Project
// Ditolve Davide 806953
// Universita' degli studi Milano Bicocca
using IronPython.Hosting;

namespace PyConnector
{
    /// <summary>
    /// This class has been designed for handling Python code
    /// within C# execution
    /// </summary>
    public class ConnectorPy
    {
        /// <summary>
        /// ATM this method call an Hello world in Python and return the string
        /// </summary>
        /// <returns>Hello world string from Python</returns>
        public static string testPy()
        {
            var engine = Python.CreateEngine();
            dynamic py = engine.ExecuteFile(@"C:\test\Hello.py");
            var pythonClass = py.Hello();
            var toReturn = pythonClass.hello_world();
            return toReturn.ToString();
        }
    }
}