# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.14] - 2020-06-03
- added obstacle hit managment
- added thesis v0.2

## [0.0.13] - 2020-05-22

- Lowered cyclomatic complexity
- Improved QLearning reward function
- Lot of fix
- DeepProfiling for C# code
- Fix missing discriminator column
- FixedSizeQueue available for usage

## [0.0.12] - 2020-05-21
- Added Test Project
- Added Queue data structure class
- Added Queue test

## [0.0.11] - 2020-05-20
- Added navmesh
- Added Waypoints
- Added Random walking script for AI
- Manager for agent collision


## [0.0.10] - 2020-05-08
- Offset is now used in AgentController not on QLearning anymore
- New DB table Envirorments
- Update SQL scripts
- HeatMap first version (need to be integrated on webapp)
- X and Y values on DB are now normalized (they start from 0 - add offset before storing)

## [0.0.9] - 2020-05-07
- Alpha and gamma are now parameters on DB
- Added Checkpoint on scene
- Managed checkpoint collision
- Managed checkpoint reward
- Added auto properties
- Added new layers
    - Checkpoint
    - Goal
    - Walkable
- Added more log
- Fixed big bug on grid position calculation
- More info on screen on play mode
- Added gizmos for minimum distance
- Managed rewards when out of walkable surface
- Unstuck when hit wall (this will help when rotation is not enough)
- Free camera movement like editor (this has been implemented before this commit)
- Fixed wrong epoch when loading exiting QTable
- Distance is now calculated with nearest point on surface

## [0.0.8] - 2020-05-03
- Fix on reward function
- Changed perception from rays to capsule
- Moved debug and test file into gitlab
- New folder for profiling
- Collision detect
- Fixed relationship between session and config
- Some new readmes
- Added comparison between profiling data

## [0.0.7] - 2020-04-29
- new fields on DB
- fix bug on reward function
- added perception for agent
- added perception for reward
- dynamic grid size from DB
- Somechanges on frontend
- Add layer manangement

## [0.0.6] - 2020-04-28
- Added screenshot function
- Fixed DLL output path
- Fixed bug on DB layer

## [0.0.5] - 2020-04-28
### Added
- Added Documentation
-- Available as PDF
-- Available on website
- Added some readmes
- Some refactoring

## History not available on GitLab but Recovered from Unity Collaborate

## [0.0.4] - 2020-04-xx

- new scene with obstacles
- 3d model 
- new scene
- new asset for exp2
- hot fix for QLearning model
- hot fix reload previous values 
- new db fields
- fix according to web project in order to be displayed
- Performance improved
- Coroutine

## [0.0.3] - 2020-03-xx

- hot fix
- qLearning
- new test scene
- fixed update 
- code comments
- new trail colors 
- new buttons fix
- Added connector python
- Added logger 
- Added speed calibration
- Added test scene

## [0.0.2] - 2020-02-xx

- update .vs projects
- Fully dynamic menu with reflection
- Reflection for DB class fields
- fix DB collation
- Fix i18n
- Created canvas with add people
- Added DB queries
- DB structure altered
- Autowalk
- Unstuck wall hit
- Added walls
- Configured DAL and DAO project
- Added DB

## [0.0.1] - 2020-01-xx
### WORK STARTED
- Initial Commit.







